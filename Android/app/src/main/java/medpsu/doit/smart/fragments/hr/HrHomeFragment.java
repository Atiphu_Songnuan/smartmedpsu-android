package medpsu.doit.smart.fragments.hr;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.MainMenu;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.hr.menu.LeaveDateFragment;
import medpsu.doit.smart.fragments.hr.menu.WorkTimeFragment;

public class HrHomeFragment extends Fragment {

    private ImageButton btnWorktime, btnLeaveDate;
    private Button btnBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        //***** Check Internet COnnection *****//
//        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_hr_home, container, false);


        btnWorktime = view.findViewById(R.id.btn_worktime);
        btnLeaveDate = view.findViewById(R.id.btn_leavedate);
        btnLeaveDate.setEnabled(false);
        btnBack = view.findViewById(R.id.btn_back);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainMenu.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        //เวลา เข้า-ออก งาน
        btnWorktime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ToastMsg("Work Time Clicked!");
                Fragment selectedFragment = new WorkTimeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Back Stack Fragment
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .add(R.id.fragment_hr_container, selectedFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        //วันลา
        btnLeaveDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment selectedFragment = new LeaveDateFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Back Stack Fragment
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .add(R.id.fragment_hr_container, selectedFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }
}
