package medpsu.doit.smart.fragments.hr.menu;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.hr.recyclerview.RecyclerViewAdapterLeaveDate;

public class LeaveDateFragment extends Fragment {

    private SwipeRefreshLayout refreshLayout;
    private Button btnBack, btnCalendar;
    private TextView textMonth;

    private List<String> leaveType, approved;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_hr_leavedate, container, false);

        refreshLayout = view.findViewById(R.id.refreshlayout);

        btnBack = view.findViewById(R.id.btn_back);
        btnCalendar = view.findViewById(R.id.btn_calendar);
        textMonth = view.findViewById(R.id.text_month);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
                        String monthTH = "";

                        switch (mMonth + 1) {
                            case 1:
                                monthTH = "มกราคม";
                                break;
                            case 2:
                                monthTH = "กุมภาพันธ์";
                                break;
                            case 3:
                                monthTH = "มีนาคม";
                                break;
                            case 4:
                                monthTH = "เมษายน";
                                break;
                            case 5:
                                monthTH = "พฤษภาคม";
                                break;
                            case 6:
                                monthTH = "มิถุนายน";
                                break;
                            case 7:
                                monthTH = "กรกฎาคม";
                                break;
                            case 8:
                                monthTH = "สิงหาคม";
                                break;
                            case 9:
                                monthTH = "กันยายน";
                                break;
                            case 10:
                                monthTH = "ตุลาคม";
                                break;
                            case 11:
                                monthTH = "พฤศจิกายน";
                                break;
                            case 12:
                                monthTH = "ธันวาคม";
                                break;
                        }
                        textMonth.setText("เดือน " + monthTH + " พ.ศ." + (mYear + 543));
                    }
                }, year, month, day);

                datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                datePickerDialog.show();
            }
        });

        String monthTH = "";
        switch (month + 1) {
            case 1:
                monthTH = "มกราคม";
                break;
            case 2:
                monthTH = "กุมภาพันธ์";
                break;
            case 3:
                monthTH = "มีนาคม";
                break;
            case 4:
                monthTH = "เมษายน";
                break;
            case 5:
                monthTH = "พฤษภาคม";
                break;
            case 6:
                monthTH = "มิถุนายน";
                break;
            case 7:
                monthTH = "กรกฎาคม";
                break;
            case 8:
                monthTH = "สิงหาคม";
                break;
            case 9:
                monthTH = "กันยายน";
                break;
            case 10:
                monthTH = "ตุลาคม";
                break;
            case 11:
                monthTH = "พฤศจิกายน";
                break;
            case 12:
                monthTH = "ธันวาคม";
                break;
        }

        textMonth.setText("เดือน " + monthTH + " พ.ศ." + (year + 543));

        leaveType = new ArrayList<>();
        leaveType.add("ลากิจ");
        leaveType.add("ลากิจ");
        leaveType.add("ลาป่วย");
        leaveType.add("ลาศึกษาต่อ");
        leaveType.add("ลาบวช");

        approved = new ArrayList<>();
        approved.add("รออนุมัติ");
        approved.add("ไม่อนุมัติ");
        approved.add("อนุมัติ");
        approved.add("อนุมัติ");
        approved.add("อนุมัติ");

        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
        final RecyclerViewAdapterLeaveDate adapter = new RecyclerViewAdapterLeaveDate(getContext(), leaveType, approved);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
