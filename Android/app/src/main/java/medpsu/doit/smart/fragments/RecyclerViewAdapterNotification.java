package medpsu.doit.smart.fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterNotification extends RecyclerView.Adapter<RecyclerViewAdapterNotification.ViewHolder> {

    private JSONArray mNotiList;

    private Context mContext;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterNotification(Context context, JSONArray notiList) {
        this.mNotiList = notiList;
        this.mContext = context;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_notification, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        try {
            holder.mTitle.setText(mNotiList.getJSONObject(position).get("title").toString());
            holder.mMessages.setText(mNotiList.getJSONObject(position).get("messages").toString());
            holder.mTime.setText(mNotiList.getJSONObject(position).get("datetime").toString());

            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Toast.makeText(mContext, mNotiList.getJSONObject(position).get("title").toString(), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mNotiList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle, mMessages, mTime;
        RelativeLayout parentLayout;

        public ViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.text_title);
            mMessages = view.findViewById(R.id.text_message);
            mTime = view.findViewById(R.id.text_time);
            parentLayout = view.findViewById(R.id.relativelayout);
        }
    }
}
