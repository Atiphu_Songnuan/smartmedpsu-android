package configs;

public class APIUtils {

    //    public static final String API_URL = "http://10.0.2.2/medpsuapp/modules/";
    public static final String API_URL = "https://medhr.medicine.psu.ac.th/app-api/";
//    public static final String API_URL = "http://10.0.2.2/smartmedpsuapi/";

    public static UserService apiService(){

        UserService userService = null;
        userService = RetrofitClient.getClient(API_URL).create(UserService.class);

        return userService;
    }
}
