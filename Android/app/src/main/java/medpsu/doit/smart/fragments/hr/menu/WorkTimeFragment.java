package medpsu.doit.smart.fragments.hr.menu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.hr.recyclerview.RecyclerViewAdapterWorkTime;
import medpsu.doit.smart.fragments.hr.recyclerview.RecyclerViewAdapterWorkTimeCurrentDate;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkTimeFragment extends Fragment {

    UserService userService;
    private int DAY = 0;
    private String MONTH = "";
    private int YEAR = 0;
    private String YM = "";

    private RelativeLayout progressRelativeLayout;
    private SwipeRefreshLayout refreshLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private TextView textCurrentTimeIn, textCurrentInDoorName;
    private RecyclerViewAdapterWorkTime adapter;
    //    private RecyclerViewAdapterWorkTimeCurrentDate currentDateAdapter;
    private CardView currentDateCard;

    private JSONArray currentDateArray;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        view = inflater.inflate(R.layout.fragment_hr_worktime, container, false);

        userService = APIUtils.apiService();
        final String headertoken = "Bearer " + UseData.getValue("access_token");

        refreshLayout = view.findViewById(R.id.refreshlayout);

        //Enable Progress Bar
        progressRelativeLayout = view.findViewById(R.id.progress_relative_layout);
        //disable the user interaction
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        collapsingToolbar = view.findViewById(R.id.collapstoolbar);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.kanit);
        collapsingToolbar.setCollapsedTitleTypeface(typeface);
        collapsingToolbar.setExpandedTitleTypeface(typeface);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.inflateMenu(R.menu.calendar_toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.calendar:
                        ShowCalendar(headertoken, year, month, day);
                        break;
                }
                return false;
            }
        });

        textCurrentTimeIn = view.findViewById(R.id.current_date_time_in_value);
        textCurrentInDoorName = view.findViewById(R.id.current_date_text_in_doorname_value);
        currentDateCard = view.findViewById(R.id.currentdate_card);

        //Get Current Date
        GetCurrentDate(headertoken);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressRelativeLayout.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                GetCurrentDate(headertoken);
                Log.d("Refresh", "Call HRTIME API");
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    public void GetCurrentDate(String token) {
        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        String selectedMonth = "";
        if ((month + 1) <= 9) {
            selectedMonth = "0" + String.valueOf(month + 1);
        } else {
            selectedMonth = String.valueOf(month + 1);
        }

        String yearmonth = "";
        yearmonth = year + "-" + selectedMonth;

        //GET Current Date, POST Month & Year
        switch (month + 1) {
            case 1:
                MONTH = "มกราคม";
                break;
            case 2:
                MONTH = "กุมภาพันธ์";
                break;
            case 3:
                MONTH = "มีนาคม";
                break;
            case 4:
                MONTH = "เมษายน";
                break;
            case 5:
                MONTH = "พฤษภาคม";
                break;
            case 6:
                MONTH = "มิถุนายน";
                break;
            case 7:
                MONTH = "กรกฎาคม";
                break;
            case 8:
                MONTH = "สิงหาคม";
                break;
            case 9:
                MONTH = "กันยายน";
                break;
            case 10:
                MONTH = "ตุลาคม";
                break;
            case 11:
                MONTH = "พฤศจิกายน";
                break;
            case 12:
                MONTH = "ธันวาคม";
                break;
        }

        YEAR = year + 543;
        YM = yearmonth;
        DAY = day;

        GetWorkTimeInMonth(token, yearmonth);
    }

    public void GetWorkTimeInMonth(final String token, final String yearmonth) {
        final Call<ResponseBody> workTime = userService.getHrTime(token, yearmonth);
        workTime.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressRelativeLayout.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    String res = response.body().string();

                    final JSONObject allDateObj = new JSONObject(res);
                    //Get Current Date data

                    if (allDateObj.getJSONArray("today").length() != 0) {
                        currentDateArray = allDateObj.getJSONArray("today");
                        textCurrentTimeIn.setText(currentDateArray.getJSONObject(0).get("time").toString());
                        textCurrentInDoorName.setText(currentDateArray.getJSONObject(0).get("door").toString());
                    } else {
                        JSONArray jsonArray = new JSONArray();
                        JSONObject todayJson = new JSONObject();
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("time", "-");
                        jsonObject.put("door", "-");
                        todayJson.put("today", jsonObject);

                        jsonArray.put(todayJson);
                        currentDateArray = jsonArray;
                        textCurrentTimeIn.setText("-");
                        textCurrentInDoorName.setText("-");
                    }

                    currentDateCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View currentDateView) {
                            SetTimeToday();
                        }
                    });

                    //Set Date History
                    JSONArray allDateList = allDateObj.getJSONArray("date_list");
                    RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
                    adapter = new RecyclerViewAdapterWorkTime(getContext(), allDateList, YEAR, MONTH);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error, Get worktime", t.toString());
                progressRelativeLayout.setVisibility(View.GONE);
                errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
            }
        });
    }

    public void SetTimeToday() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogView = layoutInflater.inflate(R.layout.alertdialog_worktime_currentdate, null);

        builder.setView(dialogView);

        ImageView btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
        RecyclerView recyclerView = dialogView.findViewById(R.id.currentdate_list);

        RecyclerViewAdapterWorkTimeCurrentDate currentAdapter = new RecyclerViewAdapterWorkTimeCurrentDate(getContext(), currentDateArray);
        currentAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(currentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Success", "Close Dialog");
                alertDialog.dismiss();
            }
        });
    }

    public void ShowCalendar(String headertoken, int year, int month, int day) {
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
                switch (mMonth + 1) {
                    case 1:
                        MONTH = "มกราคม";
                        break;
                    case 2:
                        MONTH = "กุมภาพันธ์";
                        break;
                    case 3:
                        MONTH = "มีนาคม";
                        break;
                    case 4:
                        MONTH = "เมษายน";
                        break;
                    case 5:
                        MONTH = "พฤษภาคม";
                        break;
                    case 6:
                        MONTH = "มิถุนายน";
                        break;
                    case 7:
                        MONTH = "กรกฎาคม";
                        break;
                    case 8:
                        MONTH = "สิงหาคม";
                        break;
                    case 9:
                        MONTH = "กันยายน";
                        break;
                    case 10:
                        MONTH = "ตุลาคม";
                        break;
                    case 11:
                        MONTH = "พฤศจิกายน";
                        break;
                    case 12:
                        MONTH = "ธันวาคม";
                        break;
                }
                YEAR = mYear + 543;
//                        textMonth.setText("เดือน" + MONTH + " พ.ศ." + YEAR);

                String yearmonth = "";
                String selectedMonth = "";
                if ((mMonth + 1) <= 9) {
                    selectedMonth = "0" + String.valueOf(mMonth + 1);
                } else {
                    selectedMonth = String.valueOf(mMonth + 1);
                }
                yearmonth = mYear + "-" + selectedMonth;
                System.out.println(yearmonth);
                GetWorkTimeInMonth(headertoken, yearmonth);
                progressRelativeLayout.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }, year, month, day);

        datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
        datePickerDialog.show();
    }

    private void errorDialog(String title, String check) {
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();
            }
        });
        builder.show();
    }
}
