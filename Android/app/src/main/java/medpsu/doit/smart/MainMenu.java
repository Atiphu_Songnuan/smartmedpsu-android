package medpsu.doit.smart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.fragments.HomeFragment;
import medpsu.doit.smart.fragments.MWalletFragment;
import medpsu.doit.smart.fragments.NotificationFragment;
import medpsu.doit.smart.fragments.PersonFragment;
import models.Person;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainMenu extends AppCompatActivity {

    UserService userService;
    private int BADGES_COUNT = 0;
    private BottomNavigationView bottomNavigationView;
    private BottomNavigationItemView bottomNavigationItemView;
    private View notiIcon;

    //    private ProgressDialog progressDialog;
    private RelativeLayout progressRelativeLayout;

    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;

//            NotificationBadge badge = findViewById(R.id.badges);
//            badge.setNumber(10);
            switch (menuItem.getItemId()) {
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_notification:
                    selectedFragment = new NotificationFragment();
//                    System.out.println("Badges Count: " + BADGES_COUNT);
                    notiIcon.findViewById(R.id.notificationsbadge).setVisibility(View.GONE);

//                    if (BADGES_COUNT != 0) {
//                        notiIcon.findViewById(R.id.notificationsbadge).setVisibility(View.VISIBLE);
//                        bottomNavigationItemView.addView(notiIcon);
//                    } else{
//                        notiIcon.findViewById(R.id.notificationsbadge).setVisibility(View.GONE);
//                    }

                    break;

//                case R.id.nav_ewallet:
//                    selectedFragment = new MWalletFragment();
//                    break;

                case R.id.nav_person:
                    selectedFragment = new PersonFragment();
                    break;
            }

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            fragmentTransaction.replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(MainMenu.this);

        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        progressRelativeLayout = findViewById(R.id.progress_relative_layout);
        //disable the user interaction
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        //Connect SQLite
        UseData.CreateDBKey(this.getApplicationContext());

//        final AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);
        //Get Current Version
        userService = APIUtils.apiService();

        if (UseData.getValue("access_token").equals("")) {
            Intent intent = new Intent(MainMenu.this, Login.class);
            startActivity(intent);
            finish();
//            overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
        } else {
            final String headertoken = "Bearer " + UseData.getValue("access_token");

            //Get notification Badges
            final Call<ResponseBody> badges = userService.getBadges(headertoken);
            badges.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    progressRelativeLayout.setVisibility(View.GONE);
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    try {
                        JSONObject badgesObj = new JSONObject(response.body().string());

                        BADGES_COUNT = Integer.valueOf(badgesObj.get("badge").toString());
                        bottomNavigationItemView = bottomNavigationView.findViewById(R.id.nav_notification);
                        notiIcon = LayoutInflater.from(MainMenu.this).inflate(R.layout.notification_badges, bottomNavigationItemView, false);

                        //Badges not equal 0
                        if (BADGES_COUNT != 0) {
                            TextView badgesText = notiIcon.findViewById(R.id.notificationsbadge);
                            badgesText.setVisibility(View.VISIBLE);
                            bottomNavigationItemView.addView(notiIcon);
                        } else {
                            notiIcon.findViewById(R.id.notificationsbadge).setVisibility(View.GONE);
                        }

                        //***Get User Info
                        Call<ResponseBody> textname = userService.getUserInfo(headertoken);
                        textname.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.code() == 200) {

                                    try {
                                        String res = response.body().string();
//                            System.out.println("Response: " + res);
                                        JSONObject jsonObject = new JSONObject(res);

                                        //***Check key จาก response ที่ได้จาก api หาก ket = "error" ให้ทำการโชว์ error dialog
                                        Iterator<String> iterator = jsonObject.keys();
                                        while (iterator.hasNext()) {
                                            String key = iterator.next();
                                            if (key.equals("error")) {
                                                showDialog("Error", "ไม่พบข้อมูลผู้ใช้ กรุณาติดต่อเจ้าหน้าที่");
                                            } else {
                                                Person.setName(jsonObject.get("fullname").toString());
                                                Person.setDepart(jsonObject.get("depart").toString());
                                                Person.setSex(jsonObject.get("sex").toString());

                                                //***Get User Profile
                                                Call<ResponseBody> profileImage = userService.getImage(headertoken);
                                                profileImage.enqueue(new Callback<ResponseBody>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                        progressRelativeLayout.setVisibility(View.GONE);
                                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                                        String imageType = response.headers().get("Content-Type");
                                                        //***Check Header ว่า return type กลับมาเป็น image หรือไม่
                                                        //*ถ้าใช่ทำการ setProfile
                                                        //*ถ้าไม่ใช่โชว์ dialog error
                                                        if (imageType.equals("image/jpeg")) {
                                                            InputStream inputStream = response.body().byteStream();
                                                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                                                            if (bitmap != null) {
                                                                Person.setProfile(bitmap);
                                                            } else {
                                                                Drawable drawable = null;

                                                                //Check person sex Male | Female
                                                                if (Person.getSex().equals("M")) {
                                                                    drawable = getResources().getDrawable(R.drawable.ic_user_profile_man);
                                                                } else if (Person.getSex().equals("F")) {
                                                                    drawable = getResources().getDrawable(R.drawable.ic_user_profile_woman);
                                                                }
                                                                bitmap = ((BitmapDrawable) drawable).getBitmap();
                                                                Person.setProfile(bitmap);
                                                            }

                                                            Fragment fragment = new HomeFragment();
                                                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
                                                        } else {
                                                            errorDialog("Error", "ไม่พบข้อมูลผู้ใช้ กรุณาติดต่อเจ้าหน้าที่");
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                        Log.d("Error, Get Image", t.toString());
                                                        progressRelativeLayout.setVisibility(View.GONE);
                                                        errorDialog("Error", "ไม่สามารถแสดงรูปภาพบุคคลได้");
                                                    }
                                                });
                                            }
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        progressRelativeLayout.setVisibility(View.GONE);
                                        errorDialog("Error", "ข้อมูลไม่ถูกต้อง");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        progressRelativeLayout.setVisibility(View.GONE);
                                        errorDialog("Error", "ข้อมูลไม่ถูกต้อง");
                                    }
                                } else {
                                    errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d("Error, Get Name", t.toString());
                                progressRelativeLayout.setVisibility(View.GONE);
                                errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
                            }
                        });

                    } catch (IOException e) {
                        e.printStackTrace();
                        progressRelativeLayout.setVisibility(View.GONE);
                        errorDialog("Error", "ข้อมูลไม่ถูกต้อง");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressRelativeLayout.setVisibility(View.GONE);
                        errorDialog("Error", "ข้อมูลไม่ถูกต้อง");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("Error, Get Badges", t.toString());
                }
            });
        }

        bottomNavigationView = findViewById(R.id.bottomnav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

    }

    private void errorDialog(String title, String check) {
//        System.out.println("Error: " + check);
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.show();
    }
}
