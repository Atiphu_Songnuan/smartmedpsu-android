package medpsu.doit.smart.fragments.calendar;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.MainMenu;
import medpsu.doit.smart.R;
import configs.UseData;

public class CalendarHomeFragment extends Fragment {

    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd MMM yyyy", Locale.US);
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.US);
    private SimpleDateFormat dateToUnixTime = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);

    private TextView monthName, dateName;
    private Button backBtn, neweventBtn;
    private RecyclerView recyclerView;
//    private CalendarView calendarView;

    private CompactCalendarView compactCalendarView;

    private Date selectedDate;

    private String statusName = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        final View view = inflater.inflate(R.layout.fragment_calendar_home, container, false);

        dateName = view.findViewById(R.id.date_name);
        recyclerView = view.findViewById(R.id.eventlist);
        monthName = view.findViewById(R.id.monthname);
        compactCalendarView = view.findViewById(R.id.calendar);
        compactCalendarView.setFirstDayOfWeek(Calendar.SUNDAY);

        backBtn = view.findViewById(R.id.btn_back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainMenu.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        neweventBtn = view.findViewById(R.id.btn_newevent);
        neweventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View dialogView = layoutInflater.inflate(R.layout.alertdialog_calendar_newevent, null);

                //Declare widget in worktime alert dialog
                TextView dateName;
                Spinner spinnerDropdown;
                Button btnAddEvent, btnCancel, btnCloseDialog;
                final AutoCompleteTextView titleInputEditText;

                dateName = dialogView.findViewById(R.id.currentdate_name);
                btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
                titleInputEditText = dialogView.findViewById(R.id.textview_event_title);
                spinnerDropdown = dialogView.findViewById(R.id.spinner_event_important);
                btnAddEvent = dialogView.findViewById(R.id.btn_addnewevent);
                btnCancel = dialogView.findViewById(R.id.btn_cancel_addnewevent);

                dateName.setText(dateFormatForDisplaying.format(selectedDate));

                int[] images = {R.drawable.ic_event_yellowdot, R.drawable.ic_event_reddot};
                final String[] impName = {"น้อย", "มาก"};

                CustomAdapter customAdapter = new CustomAdapter(getContext(), images, impName);
                spinnerDropdown.setAdapter(customAdapter);

                spinnerDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                        statusName = impName[position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                builder.setView(dialogView);

                final AlertDialog alertDialog = builder.create();

                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                alertDialog.setCancelable(false);
                alertDialog.show();

                btnAddEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String titleName = titleInputEditText.getText().toString();
//                        System.out.println(UseData.getValue("events"));

                        if (!titleName.equals("")) {
                            try {
                                JSONArray getAllEvent = new JSONArray();
                                JSONObject jsonObject = new JSONObject();
                                JSONArray newEventJsonArray = new JSONArray();
                                JSONObject newEventJsonObj = new JSONObject();

                                newEventJsonObj.put("data", titleName);
                                newEventJsonObj.put("status", statusName);
                                newEventJsonArray.put(newEventJsonObj);

                                //กรณีที่มี event อยู่ในวันที่เลือกแล้ว ทำการ put event ลงใน jsonObject
                                if (!UseData.getValue("events").equals("")) {
//                                JSONArray getAllEvent = new JSONArray(UseData.getValue("events").replaceAll("\\\\", ""));
                                    getAllEvent = new JSONArray(UseData.getValue("events"));
                                    boolean checkDuplicateKey = false;
                                    String duplicateKey = "";
                                    int duplicateIndex = 0;

                                    for (int i = 0; i < getAllEvent.length(); i++) {
                                        Iterator<String> iterator = getAllEvent.getJSONObject(i).keys();
                                        while (iterator.hasNext()) {
                                            String key = iterator.next();
                                            //กรณีมีข้อมูล event อยู่แล้วทำการตรวจสอบหา key ที่ตรงกับวันที่เลือกไว้
                                            //ถ้ามี ทำการเพิ่มข้อมูลเข้าไปใน วันที่นั้นโดยให้ checkDuplicatedKey มีค่า = TRUE
                                            if (selectedDate.toString().equals(key)) {
                                                checkDuplicateKey = true;
                                                duplicateKey = key;
                                                duplicateIndex = i;
                                            }
                                        }
                                    }

                                    //มีการ add event เพิ่มเข้าไปในวันที่ที่เคยทำการ add event เข้าไปแล้ว
                                    System.out.println("Before add new event: " + getAllEvent);
                                    if (checkDuplicateKey) {
//                                    System.out.println("มี key นี้อยู่แล้ว");
                                        getAllEvent.getJSONObject(duplicateIndex).getJSONArray(duplicateKey).put(newEventJsonObj);
//                                    System.out.println("Duplicated KEY After add new event: " + getAllEvent);

                                    } else { //วันที่ที่เลือกยังไม่มี event จึงทำการ add new event ลงในวันที่ที่เลือก
                                        System.out.println("ยังไม่มี key นี้");
                                        jsonObject.put(selectedDate.toString(), newEventJsonArray);
                                        getAllEvent.put(jsonObject);
//                                    System.out.println("New KEY After add new event" + getAllEvent);
                                    }

                                } else {
                                    //กรณีที่ วันที่ที่เลอกยังไม่มี event ทำการ put event ลงใน jsonArray
                                    System.out.println("Empty event");

                                    //Create new-event json array
                                    jsonObject.put(selectedDate.toString(), newEventJsonArray);
                                    getAllEvent.put(jsonObject);
//                                newEventJsonArray.put(newEventJsonObj);
                                    System.out.println("New Event Array: " + getAllEvent);
                                }

                                //Add New-Event to calendar and add New-Event to RecyclerView
                                int color = 0;
                                Date unixtime;

                                if (statusName.equals("น้อย")) {
                                    color = Color.YELLOW;
                                } else if (statusName.equals("มาก")) {
                                    color = Color.RED;
                                }

                                UpdateEvent(getAllEvent);
                                System.out.println("Event in Database: " + UseData.getValue("events"));

                                unixtime = dateToUnixTime.parse(selectedDate.toString());
                                Event event = new Event(color, unixtime.getTime(), titleName);
                                compactCalendarView.addEvent(event);

                                //set event list to Event RecyclerView
                                List<Event> eventList = compactCalendarView.getEvents(selectedDate);
                                ListEvent(eventList);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            alertDialog.dismiss();
                        } else{
                            Toast.makeText(getContext(), "กรุณาใส่ข้อความก่อน", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("Success", "Close Dialog");
                        alertDialog.dismiss();
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        });

//        compactCalendarView.removeAllEvents();
//        UseData.deleteValue("events");

        System.out.println(UseData.getValue("events"));
        if (!UseData.getValue("events").equals("")) {
            try {
                //***Get all event in DB
                JSONArray getAllEventArray = new JSONArray(UseData.getValue("events"));

                for (int i = 0; i < getAllEventArray.length(); i++) {
                    Iterator<String> iterator = getAllEventArray.getJSONObject(i).keys();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
//                        System.out.println("Event key: " + key);
                        try {
                            Object value = getAllEventArray.getJSONObject(i).get(key);
//                            System.out.println("key: " + key + "\n" + "value: " + value);

                            //***Get event in "getAllEventArray"
                            JSONArray getEventValue = new JSONArray(value.toString());
//                            System.out.println("value: " + getEventValue);

                            for (int j = 0; j < getEventValue.length(); j++) {
                                //***Get event data in "getEventValue"
                                JSONObject getEventObj = new JSONObject(getEventValue.getJSONObject(j).toString());
//                                System.out.println(getEventObj.get("data"));
                                int color = 0;
                                System.out.println("Event Status: " + getEventObj.get("status"));
                                if (getEventObj.get("status").equals("น้อย")) {
                                    color = Color.YELLOW;
                                } else if (getEventObj.get("status").equals("มาก")) {
                                    color = Color.RED;
                                }

                                Date date = dateToUnixTime.parse(key);
                                Event event = new Event(color, date.getTime(), getEventObj.get("data"));
                                compactCalendarView.addEvent(event);
                            }
                        } catch (JSONException e) {
                            // Something went wrong!
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        monthName.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        //set format เวลาใน current date ให้เป็น 00:00:00 เพื่อนำไปตรวจสอบกับ key ที่เก็บไว้ใน event array ใน database
        try {
            selectedDate = dateFormat.parse(new Date().toString());

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            selectedDate = calendar.getTime();

            dateName.setText(dateFormatForDisplaying.format(selectedDate));

            //ตรวจสอบ event ในวันที่ปัจจุบัน
            //* ถ้ามี event ให้ทำการ add ลงใน RecyclerView
            if (compactCalendarView.getEvents(selectedDate).size() != 0) {
                List<Event> eventList = compactCalendarView.getEvents(selectedDate);
                ListEvent(eventList);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(final Date dateClicked) {
                try {
                    selectedDate = dateFormat.parse(dateClicked.toString());
                    dateName.setText(dateFormatForDisplaying.format(selectedDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                Toast.makeText(getContext(), "Date selected: " + selectedDate, Toast.LENGTH_SHORT).show();
                List<Event> events = compactCalendarView.getEvents(dateClicked);
                ListEvent(events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                monthName.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                dateName.setText(dateFormatForDisplaying.format(firstDayOfNewMonth));

                try {
                    selectedDate = dateFormat.parse(firstDayOfNewMonth.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                List<Event> events = compactCalendarView.getEvents(firstDayOfNewMonth);
                ListEvent(events);
            }
        });

        return view;
    }

    //Update event in database
    public void UpdateEvent(JSONArray jsonArray) {
        String jsonArrayStr = jsonArray.toString();
        UseData.setValue("events", jsonArrayStr);
    }

    //Add event to RecyclerView
    public void ListEvent(List<Event> events) {
        RecyclerViewAdapterCalendar adapterCalendar = new RecyclerViewAdapterCalendar(getContext(), events, selectedDate);
        adapterCalendar.notifyDataSetChanged();
        recyclerView.setAdapter(adapterCalendar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
