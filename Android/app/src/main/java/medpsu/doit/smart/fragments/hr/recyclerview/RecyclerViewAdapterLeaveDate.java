package medpsu.doit.smart.fragments.hr.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterLeaveDate extends RecyclerView.Adapter<RecyclerViewAdapterLeaveDate.ViewHolder> {

    private List<String> mLeaveType, mApproved;

    private Context mContext;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterLeaveDate(Context context, List<String> leaveType, List<String> approved) {
        this.mLeaveType = leaveType;
        this.mApproved = approved;

        this.mContext = context;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_leavedate, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mLeaveType.setText(mLeaveType.get(position));

        if (mApproved.get(position).equals("อนุมัติ")) {
            holder.imgApproved.setImageResource(R.drawable.ic_approve);
//            holder.leavecardLinearlayout.setBackgroundColor(Color.parseColor("#1b8445"));
        } else if (mApproved.get(position).equals("รออนุมัติ")) {
            holder.imgApproved.setImageResource(R.drawable.ic_waitapprove);
//            holder.leavecardLinearlayout.setBackgroundColor(Color.parseColor("#fdba33"));
        } else if (mApproved.get(position).equals("ไม่อนุมัติ")) {
            holder.imgApproved.setImageResource(R.drawable.ic_notapprove);
//            holder.leavecardLinearlayout.setBackgroundColor(Color.parseColor("#ef4849"));
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "Card: " + position, Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                View dialogView = layoutInflater.inflate(R.layout.alertdialog_leavedate, null);

                TextView leaveType;
                ImageView imgApproved;
                Button btnCloseDialog;

                leaveType = dialogView.findViewById(R.id.text_leavetype_value);
                leaveType.setText(mLeaveType.get(position));

                btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);

                imgApproved = dialogView.findViewById(R.id.img_approve);
                if (mApproved.get(position).equals("อนุมัติ")) {
                    imgApproved.setImageResource(R.drawable.ic_approve);
                } else if (mApproved.get(position).equals("รออนุมัติ")) {
                    imgApproved.setImageResource(R.drawable.ic_waitapprove);
                }else if (mApproved.get(position).equals("ไม่อนุมัติ")) {
                    imgApproved.setImageResource(R.drawable.ic_notapprove);
                }

                builder.setView(dialogView);

                final AlertDialog alertDialog = builder.create();

                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                alertDialog.show();

                btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("Success", "Close Dialog");
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLeaveType.size();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mLeaveType;
        ImageView imgApproved;
        RelativeLayout parentLayout;
//        LinearLayout leavecardLinearlayout;

        public ViewHolder(View view) {
            super(view);
            mLeaveType = view.findViewById(R.id.text_leavetype_value);
            imgApproved = view.findViewById(R.id.img_approve);
            parentLayout = view.findViewById(R.id.relativelayout);
//            leavecardLinearlayout = view.findViewById(R.id.leavecardlinearlayout);
        }
    }
}
