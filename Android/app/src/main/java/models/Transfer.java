package models;

public class Transfer {

    public static String transfer_date;
    public static String transfer_detail;
    public static String transfer_money;
    public static String transfer_idinp;

    public static String getTransfer_date() {
        return transfer_date;
    }

    public static void setTransfer_date(String transfer_date) {
        Transfer.transfer_date = transfer_date;
    }

    public static String getTransfer_detail() {
        return transfer_detail;
    }

    public static void setTransfer_detail(String transfer_detail) {
        Transfer.transfer_detail = transfer_detail;
    }

    public static String getTransfer_money() {
        return transfer_money;
    }

    public static void setTransfer_money(String transfer_money) {
        Transfer.transfer_money = transfer_money;
    }

    public static String getTransfer_idinp() {
        return transfer_idinp;
    }

    public static void setTransfer_idinp(String transfer_idinp) {
        Transfer.transfer_idinp = transfer_idinp;
    }
}
