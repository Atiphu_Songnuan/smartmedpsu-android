package medpsu.doit.smart.fragments.money;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.MainMenu;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.money.menu.DebtFragment;
import medpsu.doit.smart.fragments.money.menu.SalaryFragment;
import medpsu.doit.smart.fragments.money.menu.TransferFragment;

public class MoneyHomeFragment extends Fragment {

    private Button btnBack;
    private ImageButton btnSalary, btnTransfer, btnDebt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        //***** Check Internet COnnection *****//
//        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_money_home, container, false);

        btnSalary = view.findViewById(R.id.btn_salary);
        btnSalary.setEnabled(false);

        btnTransfer = view.findViewById(R.id.btn_transfer);
        btnDebt = view.findViewById(R.id.btn_debt);

        btnBack = view.findViewById(R.id.btn_back);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainMenu.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        btnSalary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ToastMsg("Work Time Clicked!");
                Fragment selectedFragment = new SalaryFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Back Stack Fragment
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .add(R.id.fragment_money_container, selectedFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment selectedFragment = new TransferFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Back Stack Fragment
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .add(R.id.fragment_money_container, selectedFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        btnDebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectedFragment = new DebtFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Back Stack Fragment
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .add(R.id.fragment_money_container, selectedFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }
}
