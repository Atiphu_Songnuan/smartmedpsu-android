package medpsu.doit.smart.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.Login;
import medpsu.doit.smart.R;
import models.Person;

public class PersonFragment extends Fragment {

    UserService userService;

    private ImageView profileImg;
    private TextView userName, departName;
    private CardView cardLogout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_person, container, false);

        userService = APIUtils.apiService();

        profileImg = view.findViewById(R.id.profile_image);
        userName = view.findViewById(R.id.username);
        departName = view.findViewById(R.id.departname);
        cardLogout = view.findViewById(R.id.card_logout);

        profileImg.setImageBitmap(Person.getProfile());
        userName.setText(Person.getName());
        departName.setText(Person.getDepart());

//            Call<ResponseBody> profileImage = userService.getImage(headertoken);
//            profileImage.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    InputStream inputStream = response.body().byteStream();
////                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                    Bitmap bitmap = Person.getProfile();
//
//                    if (bitmap != null) {
//                        profileImg.setImageBitmap(bitmap);
//                    } else {
//                        profileImg.setImageResource(R.drawable.ic_user_profile_man);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.d("Error, Get Image", t.toString());
//                }
//            });

//            Call<ResponseBody> textname = userService.getUserInfo(headertoken);
//            textname.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    try {
//                        String res = response.body().string();
//                        JSONObject jsonObject = new JSONObject(res);
//                        userName.setText(jsonObject.get("fullname").toString());
//                        departName.setText(jsonObject.get("depart").toString());
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
////                        Log.d("Success, Get Name", jsonObject.getAsString());
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.d("Error, Get Name", t.toString());
//                }
//            });

        cardLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("ออกจากระบบ");
                builder.setMessage("ต้องการออกจากระบบใช่หรือไม่?");
                builder.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

//                            UseData.setValue("access_token", "");
                        UseData.deleteValue("access_token");
                        UseData.deleteValue("events");

//                            System.out.println("Logout: " + UseData.getValue("access_token"));
//                            System.out.println("Token Length: " + UseData.getValue("access_token").length());

                        Toast.makeText(getActivity(),
                                "ออกจากระบบสำเร็จ", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(getActivity(), Login.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        getActivity().finish();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });

        return view;
    }
}
