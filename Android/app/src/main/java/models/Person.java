package models;

import android.graphics.Bitmap;

public class Person {

    private static String perid;
    private static String name;
    private static String sex;
    private static String depart;
    private static Bitmap profile;

    public Person() {
    }

    public static String getPerid() {
        return perid;
    }

    public static void setPerid(String perid) {
        Person.perid = perid;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Person.name = name;
    }

    public static String getSex() {
        return sex;
    }

    public static void setSex(String sex) {
        Person.sex = sex;
    }

    public static String getDepart() {
        return depart;
    }

    public static void setDepart(String depart) {
        Person.depart = depart;
    }

    public static Bitmap getProfile() {
        return profile;
    }

    public static void setProfile(Bitmap profile) {
        Person.profile = profile;
    }
}
