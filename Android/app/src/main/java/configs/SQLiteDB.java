package configs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

public class SQLiteDB extends SQLiteOpenHelper {

    public enum status {
        CREATE, UPGRADE
    }

    public interface data_callback {
        void action(status status, SQLiteDatabase db);
    }

    public data_callback callback;

    public SQLiteDB(Context context, int DATABASE_VERSION, String DATABASE_NAME) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        callback.action(status.CREATE, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        callback.action(status.UPGRADE, db);
        onCreate(db);
    }

    public String getLastID(String ColumnName, String TableName){
        String sql = "SELECT "+ColumnName+" FROM "+TableName+" ORDER BY "+ColumnName+" DESC LIMIT 1";
        ArrayList<String[]> data = Select(sql, new String[]{ });
        if(data.size()>0) {
            return data.get(0)[0];
        }else{
            return "0";
        }
    }

    public ArrayList<String[]> Select(String sql, String[] value){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery(sql, value);
        ArrayList<String[]> data = new ArrayList<String[]>();
        String[] tmp;
        if(cur != null)
        {
            if (cur.moveToFirst()) {
                do {
                    tmp = new String[cur.getColumnCount()];
                    for (int i = 0; i<cur.getColumnCount(); i++){
                        tmp[i] = cur.getString(i);
                    }
                    data.add(tmp);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        Log.d("SQL ", sql);
        return data;
    }

    public long Update(String table_name, ContentValues set_value, String where, String[] where_value){
        SQLiteDatabase db = this.getWritableDatabase();
        long rows  = 0;
        try {
            rows  = db.update(table_name, set_value, where, where_value);
        }catch(Exception e){
            Log.d("SQL UPDATE ERROR", e.getMessage());
        }
        db.close();
        Log.d("SQL UPDATE", table_name);
        return rows;
    }

    public long Delete(String table_name, String where, String[] where_value){
        SQLiteDatabase db = this.getWritableDatabase();
        long rows  = 0;
        try {
            rows  = db.delete(table_name, where, where_value);
        }catch(Exception e){
            Log.d("SQL DELETE ERROR", e.getMessage());
        }
        db.close();
        Log.d("SQL DELETE", table_name);
        return rows;
    }

    public long Insert(String table_name, ContentValues value){
        SQLiteDatabase db = this.getWritableDatabase();
        long rows  = 0;
        try {
            rows  = db.insert(table_name, null, value);
        }catch(Exception e){
            Log.d("SQL INSERT ERROR", e.getMessage());
        }
        db.close();
        Log.d("SQL INSERT", table_name);
        return rows;
    }

    public void MutiInsert(String table_name, ArrayList<ContentValues> values) {
        Log.d("SQL MUTI INSERT", table_name);
        final String _table_name = table_name;
        final ArrayList<ContentValues> _values = values;
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i<_values.size(); i++){
                    ContentValues value = _values.get(i);
                    Insert(_table_name, value);
                }
            }
        }).start();
    }

    public void Exec(String sql){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(sql);
        db.close();
        Log.d("EXEC SQL", sql);
    }


}