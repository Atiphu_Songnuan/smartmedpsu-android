package medpsu.doit.smart;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import im.delight.android.webview.AdvancedWebView;

public class ForgotPassword extends Activity implements AdvancedWebView.Listener {

    private Button btnBack;
    private AdvancedWebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(ForgotPassword.this);

        setContentView(R.layout.forgotpassword);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForgotPassword.this, Login.class);
                startActivity(intent);
//                    getActivity().finish();
                ForgotPassword.this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        wv = findViewById(R.id.forgotpasswordwv);
        wv.loadUrl("https://passport.psu.ac.th/index.php?content=forgetpass");

    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        wv.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        wv.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        wv.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        wv.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!wv.onBackPressed()) {
            return;
        }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
