package configs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.json.JSONObject;

import im.delight.android.webview.AdvancedWebView;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by PKWORK on 19/4/2019.
 */


public class CustomJavaScriptInterface{

    private AdvancedWebView wv;
    private Context appContext;
    private String appURL;

    public CustomJavaScriptInterface(Context context, AdvancedWebView w){
        wv = w;
        appContext = context;
    }

    public void Open(String URL, boolean preventCaching){
        appURL = URL;
        if (isNetworkAvailable(appContext) ) {
            wv.loadUrl(appURL, preventCaching);
        }else{
            loadError();
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void loadError() {
        String html = "<style>\n" +
                "body{\n" +
                "\tpadding:0;\n" +
                "\tmargin:0;\n" +
                "\tbackground:#efeff5;\n" +
                "}\n" +
                "body, a, table, div{\n" +
                "\tcolor:#353535;\n" +
                "\tfont:1em Tahoma, Geneva, sans-serif;\n" +
                "}\n" +
                "</style>\n" +
                "\n" +
                "<html><body><table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "<tr><td>\n" +
                "\t\t<div align=\"center\"  style=\"font-size:5em;padding:0.5em;\">\n" +
                "\t\t\tNo Internet connection. Make sure Wi-Fi or Cellular data is turned on, then open app try again.<br><br>\n" +
                "\t\t \t<!--<input type=\"button\" style=\"font-size:1em;padding:0.5em;\" value=\"Retry\" onClick=\"window.location='"+appURL+"';\">-->\n" +
                "\t\t</div>\n" +
                "</td></tr>\n" +
                "</table></html></body>";
        System.out.println("html " + html);
        wv.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
        System.out.println("loaded html");
    }

    @SuppressLint("NewApi")
    @JavascriptInterface
    public void postMessage(String message) {

        String content = "";

        try {

            JSONObject json = new JSONObject(message);

            switch (json.getString("func")){
                case "ID":
                    content = UseData.getToken();
                    break;

                case "SET_VALUE":
                    content = UseData.setValue(json.getString("key"), json.getString("value"));
                    break;

                case "GET_VALUE":
                    content = UseData.getValue(json.getString("key"));
                    break;

                case "EXIT":
                    content = "true";
                    UseData.wvactivity.finish();
                    break;
            }

            content = "NativeAppCallback('"+ message +"', '" + content + "');";

            //Log.e("Build.VERSION.SDK_INT", "Level " + Build.VERSION.SDK_INT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                //wv.evaluateJavascript(content, null);
                final String c = content;
                wv.post(new Runnable() {
                    @Override
                    public void run() {
                        wv.loadUrl("javascript:" + c);
                    }
                });
            }else {
                wv.loadUrl("javascript:" +content);
            }
            Log.e("postMessage", content);

        } catch (Throwable t) {
            Log.e("Throwable", t.getMessage());
            //UseData.Alert(appContext, "Throwable", t.getMessage());
        }


    }
}
