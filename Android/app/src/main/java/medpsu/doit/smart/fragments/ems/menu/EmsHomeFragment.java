package medpsu.doit.smart.fragments.ems.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.MainMenu;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.ems.recyclerview.RecyclerViewAdapterEms;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmsHomeFragment extends Fragment {

    UserService userService;

    private RelativeLayout progressRelativeLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;

    private RecyclerViewAdapterEms adapter;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        view = inflater.inflate(R.layout.fragment_ems_home, container, false);

        userService = APIUtils.apiService();
        final String headertoken = "Bearer " + UseData.getValue("access_token");

        progressRelativeLayout = view.findViewById(R.id.progress_relative_layout);
        //disable the user interaction
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        refreshLayout = view.findViewById(R.id.refreshlayout);

        collapsingToolbar = view.findViewById(R.id.collapstoolbar);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.kanit);
        collapsingToolbar.setCollapsedTitleTypeface(typeface);
        collapsingToolbar.setExpandedTitleTypeface(typeface);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainMenu.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        GetEmsData(headertoken);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetEmsData(headertoken);
                Log.d("Refresh", "Call TRACK POST API");
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    public void GetEmsData(String token) {
        final Call<ResponseBody> trackPost = userService.getTrackPost(token);
        trackPost.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressRelativeLayout.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    String res = response.body().string();

                    JSONObject trackPostObj = new JSONObject(res);
                    JSONArray trackPostList = trackPostObj.getJSONArray("data");

                    RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
                    TextView txtEmpty = view.findViewById(R.id.txt_empty_ems);
                    if (trackPostList.length() == 0) {
                        txtEmpty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    adapter = new RecyclerViewAdapterEms(getContext(), trackPostList);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error, Get track post", t.toString());
                progressRelativeLayout.setVisibility(View.GONE);
                errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
            }
        });
    }

    private void errorDialog(String title, String check) {
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();
            }
        });
        builder.show();
    }
}
