package medpsu.doit.smart.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MWalletFragment extends Fragment {

    private static final long START_TIME_IN_MILLIS = 60000;
    //    static CountDownTimer countDownTimer = null;
    private long timeLeftInMillis = START_TIME_IN_MILLIS;

    private UserService userService;

    //    private ImageView barcodeImg, qrcodeImg;
//    private TextView barcodeText, countdowntimerText;
    private TextView balanceText;
    private Button moreDetailBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_mwallet, container, false);

        final String headertoken = "Bearer " + UseData.getValue("access_token");
        userService = APIUtils.apiService();

//        barcodeImg = view.findViewById(R.id.img_barcode);
//        qrcodeImg = view.findViewById(R.id.img_qrcode);
//        barcodeText = view.findViewById(R.id.text_barcode);
//        countdowntimerText = view.findViewById(R.id.text_countdown_timer);
        balanceText = view.findViewById(R.id.balance_value);
        moreDetailBtn = view.findViewById(R.id.btn_more_details);

//        GetOtpBarCode(headertoken);
        GetBalance(headertoken);

//        countDownTimer = new CountDownTimer(timeLeftInMillis, 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                timeLeftInMillis = millisUntilFinished;
////                UpdateCountDownText();
//            }
//
//            @Override
//            public void onFinish() {
//                GetOtpBarCode(headertoken);
//                start();
//            }
//        }.start();


        moreDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://medhr.medicine.psu.ac.th/00_pr1.html";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        return view;
    }

    public String GetOtpBarCode(String token) {
        String otpcode = "";
        final Call<ResponseBody> otp = userService.getOtpBarCode(token, "ewallet");
        otp.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
//                    GenerateBarcode(jsonObject.get("code").toString());
//                    GenerateQRCode(jsonObject.get("code").toString());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error get otp barcode", t.getMessage());
            }
        });
        return otpcode;
    }

    public void GetBalance(String token) {
        final Call<ResponseBody> balance = userService.getEWallet(token);
        balance.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    int balanceAmount = Integer.valueOf(jsonObject.get("balance_amount").toString());
                    int couponAmount = Integer.valueOf(jsonObject.get("coupon_amount").toString());
                    String balance = String.valueOf(balanceAmount + couponAmount);
                    balanceText.setText(balance + " ฿");

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error get balance", t.getMessage());
            }
        });
    }

    public void GenerateBarcode(String code) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            //Generate Barcode
            BitMatrix bitMatrixBarcode = multiFormatWriter.encode(code, BarcodeFormat.CODE_128, 1200, 400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmapBarcode = barcodeEncoder.createBitmap(bitMatrixBarcode);
//            barcodeImg.setImageBitmap(bitmapBarcode);
//            barcodeText.setText(code);
//            barcodeImg.setVisibility(View.VISIBLE);
//            barcodeImg.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void GenerateQRCode(String code) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            //Generate QRCode
            BitMatrix bitMatrixQRCode = multiFormatWriter.encode(code, BarcodeFormat.QR_CODE, 500, 500);
            BarcodeEncoder qrEncoder = new BarcodeEncoder();
            Bitmap bitmapQRCode = qrEncoder.createBitmap(bitMatrixQRCode);
//            qrcodeImg.setImageBitmap(bitmapQRCode);
//            qrcodeImg.setVisibility(View.VISIBLE);
//            qrcodeImg.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void UpdateCountDownText() {
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

//        System.out.println("Mins: " + minutes  + " Secs: "+ seconds);

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
//        countdowntimerText.setText(timeLeftFormatted);
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        countDownTimer.cancel();
//    }
}
