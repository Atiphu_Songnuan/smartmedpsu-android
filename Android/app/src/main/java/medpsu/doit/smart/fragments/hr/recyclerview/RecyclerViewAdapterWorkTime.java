package medpsu.doit.smart.fragments.hr.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterWorkTime extends RecyclerView.Adapter<RecyclerViewAdapterWorkTime.ViewHolder> {

    private JSONArray mDateList;
    private int mYear;
    private String mMonth;
    private Context mContext;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterWorkTime(Context context, JSONArray allDate, int year, String month) {
        this.mDateList = allDate;
        this.mContext = context;
        this.mYear = year;
        this.mMonth = month;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_worktime, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {

//            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            final Date date = dateFormat.parse(mDateList.getJSONObject(position).get("date").toString());
//            final Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
//
//            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
//                holder.layoutInCard.setBackgroundColor(ContextCompat.getColor(mContext, R.color.holiday));
//            } else{
//                holder.layoutInCard.setBackgroundColor(ContextCompat.getColor(mContext, R.color.workday));
//            }


            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = inFormat.parse(mDateList.getJSONObject(position).get("date").toString());
            SimpleDateFormat outFormat = new SimpleDateFormat("dd");
            String day = outFormat.format(date);

//            System.out.println("Date data: " + mDateList.getJSONObject(position).get("date").toString());

            holder.mDate.setText("วันที่ " + day + " " + mMonth + " " + mYear);

//            System.out.println("Day of Week: " + calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()));
//            System.out.println("Day of Week: " + calendar.get(Calendar.DAY_OF_WEEK));

            holder.mTimeIn.setText(mDateList.getJSONObject(position).getJSONObject("in_department").get("time").toString());
            holder.mTimeOut.setText(mDateList.getJSONObject(position).getJSONObject("out_department").get("time").toString());

            String inDoorName = mDateList.getJSONObject(position).getJSONObject("in_department").get("door").toString();
            String outDoorName = mDateList.getJSONObject(position).getJSONObject("out_department").get("door").toString();

            if (!inDoorName.equals("null")) {
                holder.mTimeInDoorName.setText(inDoorName);
            } else{
                holder.mTimeInDoorName.setText("-");
            }

            if (!outDoorName.equals("null")) {
                holder.mTimeOutDoorName.setText(outDoorName);
            } else{
                holder.mTimeOutDoorName.setText("-");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        holder.mDate.setText(mDateList);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "Card: " + position, Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                View dialogView = layoutInflater.inflate(R.layout.alertdialog_worktime, null);

                //Declare widget in worktime alert dialog
                TextView title, timeInHospital_txt, timeInHospitalDoor_txt, timeInDepart_txt, timeInDepartDoor_txt, timeOutLunch_txt, timeOutLunchDoor_txt, timeInLunch_txt, timeInLunchDoor_txt, timeOutDepart_txt, timeOutDepartDoor_txt, timeOutHospital_txt, timeOutHospitalDoor_txt;
                ImageView btnCloseDialog;

                title = dialogView.findViewById(R.id.title);
                timeInHospital_txt = dialogView.findViewById(R.id.time_in_hospital_value);
                timeInHospitalDoor_txt = dialogView.findViewById(R.id.time_in_hospital_door_value);
                timeInDepart_txt = dialogView.findViewById(R.id.time_in_depart_value);
                timeInDepartDoor_txt = dialogView.findViewById(R.id.time_in_depart_door_value);
                timeOutLunch_txt = dialogView.findViewById(R.id.time_out_lunch_value);
                timeOutLunchDoor_txt = dialogView.findViewById(R.id.time_out_lunch_door_value);
                timeInLunch_txt = dialogView.findViewById(R.id.time_in_lunch_value);
                timeInLunchDoor_txt = dialogView.findViewById(R.id.time_in_lunch_door_value);
                timeOutDepart_txt = dialogView.findViewById(R.id.time_out_depart_value);
                timeOutDepartDoor_txt = dialogView.findViewById(R.id.time_out_depart_door_value);
                timeOutHospital_txt = dialogView.findViewById(R.id.time_out_hospital_value);
                timeOutHospitalDoor_txt = dialogView.findViewById(R.id.time_out_hospital_door_value);

                try {
                    String timeInHos = mDateList.getJSONObject(position).getJSONObject("in_hospital").get("time").toString();
                    String inHospitalDoorName = mDateList.getJSONObject(position).getJSONObject("in_hospital").get("door").toString();

                    String timeOutHos = mDateList.getJSONObject(position).getJSONObject("out_hospital").get("time").toString();
                    String outHospitalDoorName = mDateList.getJSONObject(position).getJSONObject("out_hospital").get("door").toString();

                    String timeInDept = mDateList.getJSONObject(position).getJSONObject("in_department").get("time").toString();
                    String inDepartDoorName = mDateList.getJSONObject(position).getJSONObject("in_department").get("door").toString();

                    String timeOutDept = mDateList.getJSONObject(position).getJSONObject("out_department").get("time").toString();
                    String outDepartDoorName = mDateList.getJSONObject(position).getJSONObject("out_department").get("door").toString();

                    String timeInLunch = mDateList.getJSONObject(position).getJSONObject("in_lunch").get("time").toString();
                    String inLunchDoorName = mDateList.getJSONObject(position).getJSONObject("in_lunch").get("door").toString();

                    String timeOutLunch = mDateList.getJSONObject(position).getJSONObject("out_lunch").get("time").toString();
                    String outLunchDoorName = mDateList.getJSONObject(position).getJSONObject("out_lunch").get("door").toString();

                    SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date selectedDate = inFormat.parse(mDateList.getJSONObject(position).get("date").toString());
                    SimpleDateFormat outFormat = new SimpleDateFormat("dd");
                    String dayName = outFormat.format(selectedDate);
                    title.setText("วันที่ " + dayName + " " + mMonth + " " + mYear);

                    //Time In Hospital time & door name
//                    timeInHospital_txt.setText(mDateList.getJSONObject(position).getJSONObject("in_hospital").get("time").toString());
                    if (!timeInHos.equals("")) {
                        timeInHospital_txt.setText(timeInHos);
                    }else{
                        timeInHospital_txt.setText("-");
                    }

                    if (!inHospitalDoorName.equals("")) {
                        timeInHospitalDoor_txt.setText(inHospitalDoorName);
                    } else {
                        timeInHospitalDoor_txt.setText("-");
                    }

                    //Time In Department time & door name
//                    timeInDepart_txt.setText(mDateList.getJSONObject(position).getJSONObject("in_department").get("time").toString());

                    if (!timeInDept.equals("")) {
                        timeInDepart_txt.setText(timeInDept);
                    } else{
                        timeInDepart_txt.setText("-");
                    }

                    if (!inDepartDoorName.equals("")) {
                        timeInDepartDoor_txt.setText(inDepartDoorName);
                    } else {
                        timeInDepartDoor_txt.setText("-");
                    }

                    //Time Out Lunch time & door name
//                    timeOutLunch_txt.setText(mDateList.getJSONObject(position).getJSONObject("out_lunch").get("time").toString());
                    if (!timeOutLunch.equals("")) {
                        timeOutLunch_txt.setText(timeOutLunch);
                    }else{
                        timeOutLunch_txt.setText("-");
                    }
                    if (!outLunchDoorName.equals("")) {
                        timeOutLunchDoor_txt.setText(outLunchDoorName);
                    } else {
                        timeOutLunchDoor_txt.setText("-");
                    }

                    //Time In Lunch time & door name
//                    timeInLunch.setText(mDateList.getJSONObject(position).getJSONObject("in_lunch").get("time").toString());
                    if (!timeInLunch.equals("")){
                        timeInLunch_txt.setText(timeInLunch);
                    } else{
                        timeInLunch_txt.setText("-");
                    }

                    if (!inLunchDoorName.equals("")) {
                        timeInLunchDoor_txt.setText(inLunchDoorName);
                    } else {
                        timeInLunchDoor_txt.setText("-");
                    }

                    //Time Out Department time & door name
//                    timeOutDepart_txt.setText(mDateList.getJSONObject(position).getJSONObject("out_department").get("time").toString());
                    if (!timeOutDept.equals("")) {
                        timeOutDepart_txt.setText(timeOutDept);
                    } else{
                        timeOutDepart_txt.setText("-");
                    }

                    if (!outDepartDoorName.equals("")) {
                        timeOutDepartDoor_txt.setText(outDepartDoorName);
                    } else {
                        timeOutDepartDoor_txt.setText("-");
                    }

                    //Time Out Hospital time & door name
//                    timeOutHospital_txt.setText(mDateList.getJSONObject(position).getJSONObject("out_hospital").get("time").toString());
                    if (!timeOutHos.equals("")) {
                        timeOutHospital_txt.setText(timeOutHos);
                    }else{
                        timeOutHospital_txt.setText("-");
                    }
                    if (!outHospitalDoorName.equals("")) {
                        timeOutHospitalDoor_txt.setText(outHospitalDoorName);
                    } else {
                        timeOutHospitalDoor_txt.setText("-");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                builder.setView(dialogView);

                final AlertDialog alertDialog = builder.create();

                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.show();

                btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
                btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("Success", "Close Dialog");
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDateList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mDate, mTimeIn, mTimeOut, mTimeInDoorName, mTimeOutDoorName;
        RelativeLayout parentLayout, layoutInCard;

        public ViewHolder(View view) {
            super(view);
            mDate = view.findViewById(R.id.date);
            mTimeIn = view.findViewById(R.id.time_in_value);
            mTimeOut = view.findViewById(R.id.time_out_value);
            mTimeInDoorName = view.findViewById(R.id.text_in_doorname_value);
            mTimeOutDoorName = view.findViewById(R.id.text_out_doorname_value);
            parentLayout = view.findViewById(R.id.relativelayout);
            layoutInCard = view.findViewById(R.id.layout_in_card);
        }
    }
}
