package configs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class UseData {

    public static String DATABASE_NAME = "DATABASE_APP";
    public static int DATABASE_VERSION = 1;
    public static SQLiteDB DB;

    public static Activity wvactivity;

    public static void CreateDBKey(Context con){

        //Open
        DB = new configs.SQLiteDB(
                con,
                DATABASE_VERSION,
                DATABASE_NAME);

        //Create & Upgrade Database
        DB.callback = new SQLiteDB.data_callback() {
            public void action(SQLiteDB.status status, SQLiteDatabase db) {
                switch (status){

                    case CREATE:
                        db.execSQL("CREATE TABLE UserData (" +
                                "  KeyName varchar(100) NOT NULL," +
                                "  Value text NOT NULL," +
                                "  PRIMARY KEY (KeyName)" +
                                ")");
                        break;

                    case UPGRADE:
                        db.execSQL("DROP TABLE IF EXISTS UserData;");
                        break;

                    default:
                        break;
                }
            }
        };

        DB.getWritableDatabase();

    }

    public static String getValue(String key){
        ArrayList<String[]> data = DB.Select("SELECT Value FROM UserData WHERE KeyName = '"+ key +"'", null);
        if (data.size() == 0) {
            return "";
        }else{
            return data.get(0)[0];
        }
    }

    public static String setValue(String key, String value){
        if(getValue(key).length() == 0) {
            DB.Exec("INSERT INTO UserData (KeyName, Value) VALUES ('"+key+"', '"+value+"')");
        }else{
            DB.Exec("UPDATE UserData SET Value = '"+value+"' WHERE KeyName = '"+key+"'");
        }
        return getValue(key);
    }

    public static void deleteValue(String key) {
        DB.Exec("DELETE FROM UserData WHERE KeyName = '"+key+"'");
    }

    public static void Alert(Context con, String Title, String Message) {
        AlertDialog.Builder dialog_app = new AlertDialog.Builder(con);
        dialog_app.setTitle(Title);
        dialog_app.setMessage(Message);
        dialog_app.show();
    }

    public static String getToken(){
        String ID = getValue("TokenID");
        if(ID.length() == 0 || ID == null) {
            ID = FirebaseInstanceId.getInstance().getToken();
            if(ID != null){
                Log.d(TAG, "Token: " + ID);
                setValue("TokenID", ID);
            }
        }
        return ID;
    }

}
