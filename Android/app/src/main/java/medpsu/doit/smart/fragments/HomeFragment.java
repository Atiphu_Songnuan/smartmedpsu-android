package medpsu.doit.smart.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import configs.APIUtils;
import configs.UserService;
import de.hdodenhof.circleimageview.CircleImageView;
import medpsu.doit.smart.CalendarMain;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.EmsMain;
import medpsu.doit.smart.HrMain;
import medpsu.doit.smart.Login;
import medpsu.doit.smart.MoneyMain;
import medpsu.doit.smart.R;
import medpsu.doit.smart.Running;
import models.Person;

public class HomeFragment extends Fragment {

    UserService userService;

    private CircleImageView profileImg;
    private TextView username;
    private ImageButton smarthealthBtn, hrBtn, moneyBtn, calendarBtn, emsBtn, advertismentBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        userService = APIUtils.apiService();

        profileImg = view.findViewById(R.id.profile_image);
        username = view.findViewById(R.id.username);

        //Button Menu
        hrBtn = view.findViewById(R.id.btn_hr);
        calendarBtn = view.findViewById(R.id.btn_calendar);
        smarthealthBtn = view.findViewById(R.id.btn_smarthealth);
        emsBtn = view.findViewById(R.id.btn_ems);
        moneyBtn = view.findViewById(R.id.btn_money);

        //Disable Button
        advertismentBtn = view.findViewById(R.id.btn_advertisement);
        advertismentBtn.setEnabled(false);

        profileImg.setImageBitmap(Person.getProfile());
        username.setText(Person.getName());

//        try{
//            String headertoken = "Bearer " + UseData.getValue("access_token");
//            Call<ResponseBody> profileImage = userService.getImage(headertoken);
//            profileImage.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    InputStream inputStream = response.body().byteStream();
//                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//
//                    if (bitmap != null) {
//                        profileImg.setImageBitmap(bitmap);
//
//                        Person.setProfile(bitmap);
//                    } else {
//                        profileImg.setImageResource(R.drawable.ic_user_profile);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.d("Error, Get Image", t.toString());
//                }
//            });

//            Call<ResponseBody> textname = userService.getUserInfo(headertoken);
//            textname.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    try {
//                        String res = response.body().string();
//                        JSONObject jsonObject = new JSONObject(res);
//                        Person.setName(jsonObject.get("fullname").toString());
//                        Person.setDepart(jsonObject.get("depart").toString());
//
//                        username.setText(Person.getName());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
////                        Log.d("Success, Get Name", jsonObject.getAsString());
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.d("Error, Get Name", t.toString());
//                }
//            });
//        }catch (Exception e) {
//            e.printStackTrace();
//        }

        smarthealthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Running.class);
                startActivity(intent);
                //getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //HR Button
        hrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    ***** Use Fragment *****
//                    Fragment selectedFragment = new HrFragment();
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    //Back Stack Fragment
//                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                            .add(R.id.fragment_container, selectedFragment)
//                            .addToBackStack(null)
//                            .commit();

                Intent intent = new Intent(getActivity(), HrMain.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //Money Button
        moneyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    ***** Use Fragment *****
//                    Fragment selectedFragment = new MoneyFragment();
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    //Back Stack Fragment
//                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                            .add(R.id.fragment_container, selectedFragment)
//                            .addToBackStack(null)
//                            .commit();

                Intent intent = new Intent(getActivity(), MoneyMain.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //Calendar Button
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CalendarMain.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //Ems Button
        emsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EmsMain.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        return view;
    }
}
