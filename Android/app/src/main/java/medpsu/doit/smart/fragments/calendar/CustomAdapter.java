package medpsu.doit.smart.fragments.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import medpsu.doit.smart.R;

public class CustomAdapter extends BaseAdapter {

    Context context;
    int[] images;
    String[] impname;
    LayoutInflater inflater;

    public CustomAdapter(Context context, int[] img, String[] impname) {
        this.context = context;
        this.images = img;
        this.impname = impname;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.calendar_newevent_important_spinner, null);
        ImageView icon = view.findViewById(R.id.importantimage);
        TextView names = view.findViewById(R.id.importantname);
        icon.setImageResource(images[i]);
        names.setText(impname[i]);


        return view;
    }
}
