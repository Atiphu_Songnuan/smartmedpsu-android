package medpsu.doit.smart.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {

    UserService userService;

    private SwipeRefreshLayout refreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        final View view = inflater.inflate(R.layout.fragment_notification, container, false);

        userService = APIUtils.apiService();

        refreshLayout = view.findViewById(R.id.refreshlayout);

        final String headertoken = "Bearer " + UseData.getValue("access_token");

        Call<ResponseBody> notificationList = userService.getNotification(headertoken);
        notificationList.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    //Version 1
//                    JSONArray sortJsonArray = new JSONArray();
//                    for (int i = jsonArray.length()-1; i >= 0; i--) {
//                        sortJsonArray.put(jsonArray.get(i));
//                    }
//                      *************************************************************

                    //Version 2
//                    List<JSONObject> jsons = new ArrayList<>();
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        jsons.add(jsonArray.getJSONObject(i));
//                    }
//
//                    Collections.sort(jsons, new Comparator<JSONObject>() {
//                        @Override
//                        public int compare(JSONObject lhs, JSONObject rhs) {
//                            String lid = null;
//                            String rid = null;
//                            try {
//                                lid = lhs.getString("datetime");
//                                rid = rhs.getString("datetime");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            return lid.compareTo(rid);
//                        }
//                    });
//                    *************************************************************

//                    JSONArray notiJsonArray = new JSONArray(jsonArray);

                    RecyclerView recyclerView = view.findViewById(R.id.notification_list);
                    TextView txtEmpty = view.findViewById(R.id.txt_empty_ems);
                    if (jsonArray.length() == 0) {
                        txtEmpty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    final RecyclerViewAdapterNotification adapter = new RecyclerViewAdapterNotification(getContext(), jsonArray);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error, Get notification", t.toString());
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Call<ResponseBody> refreshNotiList = userService.getNotification(headertoken);
                refreshNotiList.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            JSONObject refreshJsonObject = new JSONObject(response.body().string());
                            JSONArray refreshJsonArray = refreshJsonObject.getJSONArray("data");

//                            JSONArray notiJsonArray = new JSONArray(refreshJsonArray);

                            RecyclerView refreshRecyclerView = view.findViewById(R.id.notification_list);
                            final RecyclerViewAdapterNotification adapter = new RecyclerViewAdapterNotification(getContext(), refreshJsonArray);
                            refreshRecyclerView.setAdapter(adapter);
                            refreshRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Error, Get notification", t.toString());
                    }
                });
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}
