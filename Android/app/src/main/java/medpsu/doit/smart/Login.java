package medpsu.doit.smart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    UserService userService;

    private EditText userperid, userPin, userBirthDate;
    private Button btnLogin, btnForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(Login.this);

        setContentView(R.layout.activity_login);

        userService = APIUtils.apiService();

        userperid = findViewById(R.id.userperid);
//        userBirthDate = findViewById(R.id.birthdate);
        userPin = findViewById(R.id.userpin);

        btnForgotPassword = findViewById(R.id.btn_forgot_password);
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, ForgotPassword.class);
                startActivity(intent);
//                    getActivity().finish();
                Login.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ToastMsg(userBirthDate.getText().toString());

                Call<ResponseBody> call = userService.getToken(userperid.getText().toString(), userPin.getText().toString());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String res = response.body().string();
                            JSONObject jsonObject = new JSONObject(res);

                            if (jsonObject.has("access_token")) {
                                String token = jsonObject.get("access_token").toString();
                                ToastMsg("เข้าสู่ระบบสำเร็จ");
                                UseData.setValue("access_token", token);
                                Intent intent = new Intent(Login.this, MainMenu.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            } else {
                                String errorMsg = jsonObject.get("error").toString();
//                                ToastMsg("รหัสบุคลากร หรือ pin ไม่ถูกต้อง, กรุณา activity_login ใหม่อีกครั้ง!");
                                ToastMsg(errorMsg);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("Error", t.toString());
                        errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
                    }
                });
            }
        });
    }

    public void ToastMsg(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();
    }

    private void errorDialog(String title, String check) {
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.show();
    }
}