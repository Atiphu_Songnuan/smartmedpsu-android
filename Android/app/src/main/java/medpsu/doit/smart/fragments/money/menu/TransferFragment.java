package medpsu.doit.smart.fragments.money.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.money.recyclerview.RecyclerViewAdapterTransfer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferFragment extends Fragment {

    UserService userService;

    private RelativeLayout progressRelativeLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;

    private RecyclerViewAdapterTransfer adapter;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        view = inflater.inflate(R.layout.fragment_money_transfer, container, false);

        userService = APIUtils.apiService();
        final String headertoken = "Bearer " + UseData.getValue("access_token");

        progressRelativeLayout = view.findViewById(R.id.progress_relative_layout);
        //disable the user interaction
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        refreshLayout = view.findViewById(R.id.refreshlayout);

        collapsingToolbar = view.findViewById(R.id.collapstoolbar);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.kanit);
        collapsingToolbar.setCollapsedTitleTypeface(typeface);
        collapsingToolbar.setExpandedTitleTypeface(typeface);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.inflateMenu(R.menu.calendar_toolbar);

        //Set back button in toolbar
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.calendar:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View dialogView = layoutInflater.inflate(R.layout.alertdialog_calendar, null);

                        DatePicker startDatePicker = dialogView.findViewById(R.id.start_date);
                        DatePicker endDatePicker = dialogView.findViewById(R.id.end_date);

                        builder.setView(dialogView);

                        final AlertDialog alertDialog = builder.create();

                        ImageView btnCloseDialog;
                        btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);

                        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.d("Success", "Close Dialog");
                                alertDialog.dismiss();
                            }
                        });

                        Button confirmBtn = dialogView.findViewById(R.id.btn_confirm);
                        confirmBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String selectedStartMonth = "";
                                String selectedStartDay = "";
                                String selectedEndMonth = "";
                                String selectedEndDay = "";

                                //Reformat Start Date
                                if ((startDatePicker.getMonth() + 1) <= 9) {
                                    selectedStartMonth = "0" + String.valueOf(startDatePicker.getMonth() + 1);
                                } else {
                                    selectedStartMonth = String.valueOf(startDatePicker.getMonth() + 1);
                                }

                                if (startDatePicker.getDayOfMonth() <= 9) {
                                    selectedStartDay = "0" + String.valueOf(startDatePicker.getDayOfMonth());
                                } else {
                                    selectedStartDay = String.valueOf(startDatePicker.getDayOfMonth());
                                }
                                //*********************************************************************

                                //Reformat End Date
                                if ((endDatePicker.getMonth() + 1) <= 9) {
                                    selectedEndMonth = "0" + String.valueOf(endDatePicker.getMonth() + 1);
                                } else {
                                    selectedEndMonth = String.valueOf(endDatePicker.getMonth() + 1);
                                }

                                if (endDatePicker.getDayOfMonth() <= 9) {
                                    selectedEndDay = "0" + String.valueOf(endDatePicker.getDayOfMonth());
                                } else {
                                    selectedEndDay = String.valueOf(endDatePicker.getDayOfMonth());
                                }
                                //*********************************************************************

                                String prevMonth = startDatePicker.getYear() + "-" + selectedStartMonth + "-" + selectedStartDay;
                                String curMonth = startDatePicker.getYear() + "-" + selectedEndMonth + "-" + selectedEndDay;

                                Locale locale = new Locale("TH");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", locale);

                                try {
                                    Date date1 = sdf.parse(prevMonth);
                                    Date date2 = sdf.parse(curMonth);

                                    if (date1.before(date2)) {
                                        GetTransferData(headertoken, prevMonth, curMonth);
                                        alertDialog.dismiss();
                                        progressRelativeLayout.setVisibility(View.VISIBLE);
                                        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    } else{
                                        Toast.makeText(getContext(), "กรุณาเลือกวันที่ให้ถูกต้อง", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        alertDialog.show();
                        break;
                }
                return false;
            }
        });

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Calendar currentCalendar = Calendar.getInstance();
        String currentMonth = df.format(currentCalendar.getTime());

        Calendar previousCalendar = Calendar.getInstance();
        previousCalendar.add(Calendar.MONTH, -6);
        String previousMonth = df.format(previousCalendar.getTime());

        GetTransferData(headertoken, previousMonth, currentMonth);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                progressRelativeLayout.setVisibility(View.VISIBLE);
//                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                GetTransferData(headertoken, previousMonth, currentMonth);
                Log.d("Refresh", "Call TRANSFER API");
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    public void GetTransferData(String token, String previousMonth, String currentMonth) {

//        System.out.println("Current Date: " + currentMonth + " => " + "Previous Date: " + previousMonth);

        final Call<ResponseBody> transfer = userService.getTransfer(token, previousMonth, currentMonth);
        transfer.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressRelativeLayout.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                try {
                    String res = response.body().string();
                    System.out.println("Transfer Response: " + res);
                    final JSONObject allTransferObj = new JSONObject(res);
                    JSONArray allTransferList = allTransferObj.getJSONArray("data");

                    TextView txtEmpty = view.findViewById(R.id.txt_empty_transfer);
                    RecyclerView recyclerView = view.findViewById(R.id.recyclerview);

                    if (allTransferList.length() == 0){
                        txtEmpty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else{
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    adapter = new RecyclerViewAdapterTransfer(getContext(), getFragmentManager(), allTransferList);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error, Get transfer", t.toString());
                progressRelativeLayout.setVisibility(View.GONE);
                errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
            }
        });
    }

    private void errorDialog(String title, String check) {
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();
            }
        });
        builder.show();
    }
}
