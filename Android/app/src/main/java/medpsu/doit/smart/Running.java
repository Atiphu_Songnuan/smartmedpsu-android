package medpsu.doit.smart;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.WindowManager;

import configs.CustomJavaScriptInterface;
import configs.UseData;
import im.delight.android.webview.AdvancedWebView;

public class Running extends Activity implements AdvancedWebView.Listener {

    private AdvancedWebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(Running.this);

        setContentView(R.layout.running);

        //Set Activity ไว้สำหรับปุ้ม Back เมื่อกด Back ให้ Set wvactivity.finish() เพื่อปิดหน้าเมนู Running
        UseData.wvactivity = Running.this;

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        wv = findViewById(R.id.awv);
        wv.setListener(this, this);
        wv.getSettings().setSupportMultipleWindows(false);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.addPermittedHostname("psu.ac.th");

        CustomJavaScriptInterface SJscript = new CustomJavaScriptInterface(this, wv);
        wv.addJavascriptInterface(SJscript, "messageHandlers");

        boolean preventCaching = false;
        SJscript.Open("https://medhr.medicine.psu.ac.th/app-api/running/index.php", preventCaching);


    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        wv.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        wv.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        wv.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        wv.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!wv.onBackPressed()) {
            return;
        }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
