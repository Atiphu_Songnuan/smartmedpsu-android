package medpsu.doit.smart.fragments.calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import medpsu.doit.smart.R;
import configs.UseData;

public class RecyclerViewAdapterCalendar extends RecyclerView.Adapter<RecyclerViewAdapterCalendar.ViewHolder> {

    private List<Event> mEvents;
    private Context mContext;
    private Date mSelectedDate;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterCalendar(Context context, List<Event> events, Date selectedDate) {
        this.mContext = context;
        this.mEvents = events;
        this.mSelectedDate = selectedDate;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendar_events, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final String eventsData = mEvents.get(position).getData().toString();

        //YELLOW COLOR
        if (mEvents.get(position).getColor() == -256) {
            holder.mImageStatus.setImageResource(R.drawable.ic_event_yellowdot);
        }
        //RED COLOR
        else if (mEvents.get(position).getColor() == -65536) {
            holder.mImageStatus.setImageResource(R.drawable.ic_event_reddot);
        }

        holder.mEventTextView.setText(eventsData);
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(mContext);
                builder.setMessage("ลบ event");
                builder.setPositiveButton("ลบ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            JSONArray jsonArray = new JSONArray(UseData.getValue("events"));
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Iterator<String> iterator = jsonArray.getJSONObject(i).keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    if (mSelectedDate.toString().equals(key)) {
                                        if (jsonArray.getJSONObject(i).getJSONArray(mSelectedDate.toString()).length() > 1) {
                                            jsonArray.getJSONObject(i).getJSONArray(mSelectedDate.toString()).remove(position);
                                            break;
                                        } else {
                                            jsonArray.remove(i);
                                            break;
                                        }
                                    }
                                }
                            }

                            DeleteEvent(position);

//                            System.out.println("Before remove events DB: " + UseData.getValue("events"));
//                            System.out.println("Before json array: " + jsonArray);
                            if (jsonArray.length() != 0) {
                                UpdateEvent(jsonArray);
                            } else {
                                UseData.deleteValue("events");
                            }
//                            System.out.println("After remove events DB: " + UseData.getValue("events"));
//                            System.out.println("After json array: " + jsonArray);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(mContext,
                                "ลบสำเร็จ", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
////                Toast.makeText(mContext, "Card: " + position, Toast.LENGTH_SHORT).show();
//
//                AlertDialog.Builder builder =
//                        new AlertDialog.Builder(mContext);
//                builder.setMessage("ต้องการลบเหตุการณ์นี้?");
//                builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        try {
//                            JSONArray jsonArray = new JSONArray(UseData.getValue("events"));
//                            boolean checkDuplicateKey = false;
//                            String duplicateKey = "";
//                            int eventIndex = 0, eventIndexRemove = 0;
//
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                Iterator<String> iterator = jsonArray.getJSONObject(i).keys();
//                                while (iterator.hasNext()) {
//                                    String key = iterator.next();
//
//                                    if (mSelectedDate.toString().equals(key)) {
//                                        checkDuplicateKey = true;
//                                        duplicateKey = key;
//                                        eventIndex = i;
//                                        eventIndexRemove = position;
////                                        System.out.println("Event key: " + key);
////                                        System.out.println("Select Date: " + mSelectedDate);
////                                        JSONObject jsonObject = new JSONObject(jsonArray.getJSONObject(i).get(key).toString());
////                                        System.out.println("JSON OBject: " + jsonObject);
////                                        Object value = jsonArray.getJSONObject(i).get(key);
////                                        newJsonAray = new JSONArray(value.toString());
////                                        newJsonAray.remove(position);
////                                        System.out.println("After: " + newJsonAray);
//
////                                        System.out.println("Event: " + mCompactCalendarView.getEvents(mSelectedDate));
//                                    }
//                                }
//                            }
//
//                            if (checkDuplicateKey) {
//                                System.out.println("Update event!");
//                                System.out.println("Before: " + UseData.getValue("events"));
//
////                                if (jsonArray.getJSONObject(eventIndex).getJSONArray(duplicateKey).length() == 0) {
////
////                                }
//                                System.out.println(jsonArray);
//                                System.out.println("Event size: " + jsonArray.getJSONObject(eventIndex).getJSONArray(duplicateKey).getJSONObject(eventIndexRemove).length());
//
//                                if (jsonArray.getJSONObject(eventIndex).getJSONArray(duplicateKey).length() == 0) {
//                                    System.out.println("ลบ event วันที่ " + duplicateKey);
//                                } else{
//                                    System.out.println("ลบข้อมูลใน event");
//                                }
//
////                                System.out.println(jsonArray.getJSONObject(eventIndex).remove(duplicateKey));
////                                jsonArray.getJSONObject(eventIndex).getJSONArray(duplicateKey).remove(eventIndexRemove);
//
//
////                                UseData.setValue("events", jsonArray.toString());
////                                System.out.println("After: " + UseData.getValue("events"));
//
////                                mCompactCalendarView.removeEvent(mEvents.get(position));
//
//                                //Remove item in RecyclerView
////                                notifyItemRemoved(position);
//                                Toast.makeText(mContext,
//                                        "ลบเหตุการณ์สำเร็จ!", Toast.LENGTH_SHORT).show();
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        dialog.dismiss();
//
//                    }
//                });
//                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    //Update event in database
    public void UpdateEvent(JSONArray jsonArray) {
        String jsonArrayStr = jsonArray.toString();
        UseData.setValue("events", jsonArrayStr);
    }

    public void DeleteEvent(int position) {
        mEvents.remove(position);
        notifyItemRemoved(position);
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageStatus;
        TextView mEventTextView;
        RelativeLayout parentLayout;

        public ViewHolder(View view) {
            super(view);
            mImageStatus = view.findViewById(R.id.img_status);
            mEventTextView = view.findViewById(R.id.eventname);
            parentLayout = view.findViewById(R.id.relativelayout);
        }
    }
}
