package configs;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserService {

//    @FormUrlEncoded
//    @POST("insert.php")
//    Observable<User> insertUser(@Field("perid") String userperid, @Field("password") String userpassword);

//    @POST("insert.php")
//    Call<User> createUser(@Body User user);

//    @FormUrlEncoded
//    @POST("select.php")
//    Call<List<Token>> getUser(@Field("PERID") String userperid, @Field("PIN") String userpin);
//    Call<Token> getUser(@Field("id") String userperid, @Field("passwd") String userpin);

//**************************************************************************************************************//

    // ***** From Server *****

    //***GET***//
    @GET("v1/?/display")
    Call<ResponseBody> getImage(@Header("Authorization") String access_token);

    @GET("v1/?/info")
    Call<ResponseBody> getUserInfo(@Header("Authorization") String access_token);

    @GET("v1/?/notification/data")
    Call<ResponseBody> getNotification(@Header("Authorization") String access_token);

    @GET("v1/?/notification/badge")
    Call<ResponseBody> getBadges(@Header("Authorization") String access_token);

    @GET("v1/?/track_post")
    Call<ResponseBody> getTrackPost(@Header("Authorization") String access_token);

    @GET("dev/?/debt")
    Call<ResponseBody> getDebt(@Header("Authorization") String access_token);

    @GET("v1/?/ewallet")
    Call<ResponseBody> getEWallet(@Header("Authorization") String access_token);

    //***POST***//
    @FormUrlEncoded
    @POST("v1/?/access")
    Call<ResponseBody> getToken(@Field("id") String userperid, @Field("passwd") String userpin);

    @FormUrlEncoded
    @POST("v1/?/hrtime")
    Call<ResponseBody> getHrTime(@Header("Authorization") String access_token, @Field("date") String yearmonth);

    @FormUrlEncoded
    @POST("v1/?/transfer")
    Call<ResponseBody> getTransfer(@Header("Authorization") String access_token, @Field("date_fr") String date_fr, @Field("date_ex") String date_ex);

    @FormUrlEncoded
    @POST("v1/?/transferhistory")
    Call<ResponseBody> getTransferHistory(@Header("Authorization") String access_token, @Field("fid_inp") String fid_inp);

    @FormUrlEncoded
    @POST("v1/?/otp")
    Call<ResponseBody> getOtpBarCode(@Header("Authorization") String access_token, @Field("module") String module);

    //  ***** Localhost *****
//    @GET("?/notification/data")
//    Call<ResponseBody> getNotification(@Header("Authorization") String access_token);
//
//    @GET("?/notification/badge")
//    Call<ResponseBody> getBadges(@Header("Authorization") String access_token);
//
//    @FormUrlEncoded
//    @POST("?/transfer")
//    Call<ResponseBody> getTransfer(@Header("Authorization") String access_token, @Field("date_fr") String date_fr, @Field("date_ex") String date_ex);
//
//    @FormUrlEncoded
//    @POST("?/transferhistory")
//    Call<ResponseBody> getTransferHistory(@Header("Authorization") String access_token, @Field("fid_inp") String fid_inp);
//
//    @FormUrlEncoded
//    @POST("?/dept")
//    Call<ResponseBody> getDept(@Header("Authorization") String access_token);
//
//    @FormUrlEncoded
//    @POST("?/access")
//    Call<Token> getToken(@Field("id") String userperid, @Field("passwd") String userpin);
//
//    @GET("?/display")
//    Call<ResponseBody> getImage(@Header("Authorization") String access_token);
//
//    @GET("?/info")
//    Call<ResponseBody> getUserInfo(@Header("Authorization") String access_token);
//
//    @FormUrlEncoded
//    @POST("?/hrtime")
//    Call<ResponseBody> getHrTime(@Header("Authorization") String access_token, @Field("date") String yearmonth);
//
//    @GET("?/track_post")
//    Call<ResponseBody> getTrackPost(@Header("Authorization") String access_token);
//
//    @FormUrlEncoded
//    @POST("?/otp")
//    Call<ResponseBody> getOtpBarCode(@Header("Authorization") String access_token, @Field("module") String module);
//
//    @GET("?/ewallet")
//    Call<ResponseBody> getEWallet(@Header("Authorization") String access_token);

//    @GET("?/notification/data")
//    Call<ResponseBody> getNotification(@Header("Authorization") String access_token);
//
//    @GET("?/notification/badge")
//    Call<ResponseBody> getBadges(@Header("Authorization") String access_token);
}
