package medpsu.doit.smart.fragments.hr.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterWorkTimeCurrentDate extends RecyclerView.Adapter<RecyclerViewAdapterWorkTimeCurrentDate.ViewHolder> {

    private Context mContext;
    private JSONArray mCurrentList;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterWorkTimeCurrentDate(Context context, JSONArray currentDate) {
        this.mContext = context;
        this.mCurrentList = currentDate;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_worktime_currentdate, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if (position == 0) {
            holder.mMarkerIcon.setImageResource(R.drawable.ic_action_clock);
        }
        try {
            holder.mTime.setText(mCurrentList.getJSONObject(position).get("time").toString());
            holder.mDoor.setText(mCurrentList.getJSONObject(position).get("door").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "Card: " + position, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return mCurrentList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mMarkerIcon;
        TextView mTime, mDoor;
        RelativeLayout parentLayout;

        public ViewHolder(View view) {
            super(view);
            parentLayout = view.findViewById(R.id.relativelayout);
            mMarkerIcon = view.findViewById(R.id.marker);
            mTime = view.findViewById(R.id.text_time);
            mDoor = view.findViewById(R.id.text_door_name);
        }
    }
}
