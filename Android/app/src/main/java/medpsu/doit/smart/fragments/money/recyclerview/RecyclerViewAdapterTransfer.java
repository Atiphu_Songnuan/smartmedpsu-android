package medpsu.doit.smart.fragments.money.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.money.menu.TransferHistoryFragment;
import models.Transfer;

public class RecyclerViewAdapterTransfer extends RecyclerView.Adapter<RecyclerViewAdapterTransfer.ViewHolder> {

    private JSONArray mTransferList;
    private Context mContext;
    private FragmentManager mFragmentManager;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterTransfer(Context context, FragmentManager fragmentManager, JSONArray allTransfer) {
        this.mTransferList = allTransfer;
        this.mContext = context;
        this.mFragmentManager = fragmentManager;

    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_money_transfer, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            holder.transferDate.setText(mTransferList.getJSONObject(position).get("transfer_date").toString());
            holder.transferDetail.setText(mTransferList.getJSONObject(position).get("detail").toString());
            holder.transferMoney.setText(mTransferList.getJSONObject(position).get("balance").toString() + " ฿");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
//                    Toast.makeText(mContext, mTransferList.getJSONObject(position).get("transfer_date").toString(), Toast.LENGTH_SHORT).show();
                    String transferIdInp = mTransferList.getJSONObject(position).get("id_inp").toString();
//                    String transferDate = mTransferList.getJSONObject(position).get("transfer_date").toString();
//                    String transferDetail = mTransferList.getJSONObject(position).get("detail").toString();
//                    String transferMoney = mTransferList.getJSONObject(position).get("balance").toString();

                    Transfer transferData = new Transfer();
//                    transferData.setTransfer_date(transferDate);
//                    transferData.setTransfer_detail(transferDetail);
//                    transferData.setTransfer_money(transferMoney);
                    transferData.setTransfer_idinp(transferIdInp);

                    Fragment selectedFragment = new TransferHistoryFragment();
                    FragmentManager fragmentManager = mFragmentManager;
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    //Back Stack Fragment
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .add(R.id.fragment_money_container, selectedFragment)
                            .addToBackStack(null)
                            .commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mTransferList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout parentLayout;
        TextView transferDate, transferDetail, transferMoney;

        public ViewHolder(View view) {
            super(view);
            parentLayout = view.findViewById(R.id.relativelayout);
            transferDate = view.findViewById(R.id.transfer_date);
            transferDetail = view.findViewById(R.id.transfer_detail);
            transferMoney = view.findViewById(R.id.transfer_money);
        }
    }
}
