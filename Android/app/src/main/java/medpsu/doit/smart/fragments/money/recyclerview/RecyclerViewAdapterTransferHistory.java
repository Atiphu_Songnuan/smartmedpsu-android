package medpsu.doit.smart.fragments.money.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterTransferHistory extends RecyclerView.Adapter<RecyclerViewAdapterTransferHistory.ViewHolder> {

    private JSONArray mTransferHistory;
    private Context mContext;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterTransferHistory(Context context, JSONArray transferhistorylist) {
        this.mTransferHistory = transferhistorylist;
        this.mContext = context;

    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_money_transfer_history, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            holder.transfer_date.setText(mTransferHistory.getJSONObject(position).get("date_tran").toString());
            holder.depart.setText(mTransferHistory.getJSONObject(position).get("processor_name").toString());
            holder.process.setText(mTransferHistory.getJSONObject(position).get("process_name").toString());

            String tonametext = mTransferHistory.getJSONObject(position).get("to_name").toString();
            if (tonametext != "null") {
                holder.toName.setVisibility(View.VISIBLE);
                holder.toName.setText(tonametext);
            } else{
                holder.toName.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mTransferHistory.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout parentLayout;
        TextView transfer_date, depart, process, toName;

        public ViewHolder(View view) {
            super(view);
            parentLayout = view.findViewById(R.id.relativelayout);
            transfer_date = view.findViewById(R.id.text_transfer_datetime);
            depart = view.findViewById(R.id.title_depart_value);
            process = view.findViewById(R.id.text_process);
            toName = view.findViewById(R.id.text_to);
        }
    }
}
