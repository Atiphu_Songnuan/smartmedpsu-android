package medpsu.doit.smart.fragments.money.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import configs.APIUtils;
import configs.UseData;
import configs.UserService;
import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.money.recyclerview.RecyclerViewAdapterTransferHistory;
import models.Transfer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferHistoryFragment extends Fragment {

    private UserService userService;

    private RelativeLayout progressRelativeLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerViewAdapterTransferHistory adapter;
    private View view;

    private JSONArray transferinfo, transferprocess;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        view = inflater.inflate(R.layout.fragment_money_transfer_history, container, false);
        userService = APIUtils.apiService();
        final String headertoken = "Bearer " + UseData.getValue("access_token");

        progressRelativeLayout = view.findViewById(R.id.progress_relative_layout);
        //disable the user interaction
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        refreshLayout = view.findViewById(R.id.refreshlayout);

        collapsingToolbar = view.findViewById(R.id.collapstoolbar);
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.kanit);
        collapsingToolbar.setCollapsedTitleTypeface(typeface);
        collapsingToolbar.setExpandedTitleTypeface(typeface);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.inflateMenu(R.menu.money_transferhistory_toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.info:
                        ShowInfoDialog();
                        break;
                    case R.id.document:
                        ShowDocumentDialog();
                        break;
                }
                return false;
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetTransferHistoryData(headertoken, Transfer.getTransfer_idinp());
                Log.d("Refresh", "Call TRANSFER HISTORY API");
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
            }
        });

        GetTransferHistoryData(headertoken, Transfer.getTransfer_idinp());

        return view;
    }

    public void GetTransferHistoryData(String token, String id_inp){
        final Call<ResponseBody> transfer = userService.getTransferHistory(token, id_inp);
        transfer.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressRelativeLayout.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    String res = response.body().string();

//                    System.out.println(res);
                    final JSONObject allTransferObj = new JSONObject(res);

                    //Get Transfer Info
                    transferinfo = allTransferObj.getJSONObject("data").getJSONArray("info");

                    //Get Transfer Process
                    transferprocess = allTransferObj.getJSONObject("data").getJSONArray("process");

                    //Get Transfer History
                    JSONArray allTransferHistoryList = allTransferObj.getJSONObject("data").getJSONArray("history");
                    RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
                    adapter = new RecyclerViewAdapterTransferHistory(getContext(), allTransferHistoryList);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error, Get transfer", t.toString());
                progressRelativeLayout.setVisibility(View.GONE);
                errorDialog("ไม่สามารถเชื่อมต่ออินเทอร์เน็ตได้", "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตของคุณแล้วลองอีกครั้ง");
            }
        });
    }

    public void ShowInfoDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogView = layoutInflater.inflate(R.layout.alertdialog_transfer_history_info, null);

        TextView txtDocId, txtDate, txtTitle, txtDepart;

        txtDocId = dialogView.findViewById(R.id.text_document_id_value);
        txtDate = dialogView.findViewById(R.id.text_date_stamp_value);
        txtTitle = dialogView.findViewById(R.id.text_title_value);
        txtDepart = dialogView.findViewById(R.id.text_from_depart_value);

        try {
            txtDocId.setText(transferinfo.getJSONObject(0).get("doc_id").toString());
            txtDate.setText(transferinfo.getJSONObject(0).get("date_doc").toString());
            txtTitle.setText(transferinfo.getJSONObject(0).get("title").toString());
            txtDepart.setText(transferinfo.getJSONObject(0).get("dept_from").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();

        ImageView btnCloseDialog;
        btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Success", "Close Dialog");
                alertDialog.dismiss();
            }
        });

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    public void ShowDocumentDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogView = layoutInflater.inflate(R.layout.alertdialog_transfer_history_process, null);

        TextView txtDocId, txtProcessName, txtDate, txtProceedBy;

        txtDocId = dialogView.findViewById(R.id.text_document_id_value);
        txtDate = dialogView.findViewById(R.id.text_date_stamp_value);
        txtProcessName = dialogView.findViewById(R.id.text_title_value);
        txtProceedBy = dialogView.findViewById(R.id.text_process_by_value);

        try {
            txtDocId.setText(transferprocess.getJSONObject(0).get("fid_inp").toString());
            txtDate.setText(transferprocess.getJSONObject(0).get("date_inp").toString());
            txtProcessName.setText(transferprocess.getJSONObject(0).get("process_name").toString());
            txtProceedBy.setText(transferprocess.getJSONObject(0).get("process_by").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();

        ImageView btnCloseDialog;
        btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Success", "Close Dialog");
                alertDialog.dismiss();
            }
        });

        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    private void errorDialog(String title, String check) {
        showDialog(title, check);
    }

    private void showDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                getActivity().finish();
            }
        });
        builder.show();
    }
}
