package medpsu.doit.smart.fragments.money.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import medpsu.doit.smart.R;
import medpsu.doit.smart.fragments.money.menu.TransferHistoryFragment;
import models.Transfer;

public class RecyclerViewAdapterDebt extends RecyclerView.Adapter<RecyclerViewAdapterDebt.ViewHolder> {

    private JSONArray mTransferList;
    private Context mContext;
    private FragmentManager mFragmentManager;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterDebt(Context context, FragmentManager fragmentManager, JSONArray allDebt) {
        this.mTransferList = allDebt;
        this.mContext = context;
        this.mFragmentManager = fragmentManager;

    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_money_debt, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            holder.finalDate.setText("ครบกำหนดชำระคืน (" + mTransferList.getJSONObject(position).get("date_final").toString() + ")");
            holder.debtDetail.setText(mTransferList.getJSONObject(position).get("debt_detail").toString());
            holder.summary.setText(mTransferList.getJSONObject(position).get("summary_money").toString() + " ฿");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mTransferList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout parentLayout;
        TextView finalDate, debtDetail, summary;

        public ViewHolder(View view) {
            super(view);
            parentLayout = view.findViewById(R.id.relativelayout);
            finalDate = view.findViewById(R.id.final_date);
            debtDetail= view.findViewById(R.id.debt_detail);
            summary = view.findViewById(R.id.summary_money);
        }
    }
}
