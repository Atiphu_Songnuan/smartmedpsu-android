package medpsu.doit.smart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import medpsu.doit.smart.fragments.money.MoneyHomeFragment;

public class MoneyMain extends AppCompatActivity {

    private static String TAG = "Money Main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        //***** Check Internet COnnection *****//
//        CheckNetWorkConnection.isNetworkAvailable(MoneyMain.this);

        setContentView(R.layout.activity_money);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        Fragment fragment = new MoneyHomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_money_container, fragment).commit();

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MoneyMain", "Back to Previous Fragment");
            fm.popBackStack();
        } else {
            Log.i("MoneyMain", "Back to MainMenu");
            Intent intent = new Intent(MoneyMain.this, MainMenu.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            super.onBackPressed();
        }
    }
}
