package medpsu.doit.smart.fragments.money.menu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import medpsu.doit.smart.CheckNetWorkConnection;
import medpsu.doit.smart.R;

public class SalaryFragment extends Fragment {

    private Button btnBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //***** Check Internet COnnection *****//
        CheckNetWorkConnection.isNetworkAvailable(getContext());

        View view = inflater.inflate(R.layout.fragment_money_salary, container, false);


//        refreshLayout = view.findViewById(R.id.refreshlayout);

        btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.popBackStack();
            }
        });
//
//        emsList = new ArrayList<>();
//        emsList.add("พัสดุ");
//        emsList.add("พัสดุ");
//        emsList.add("จดหมาย");
//
//        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
//        final RecyclerViewAdapterEms adapter = new RecyclerViewAdapterEms(getContext(), emsList);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//
//        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                adapter.notifyDataSetChanged();
//                refreshLayout.setRefreshing(false);
//                Toast.makeText(getContext(), "อัพเดทข้อมูลล่าสุดแล้ว", Toast.LENGTH_SHORT).show();
//            }
//        });
        return view;
    }
}
