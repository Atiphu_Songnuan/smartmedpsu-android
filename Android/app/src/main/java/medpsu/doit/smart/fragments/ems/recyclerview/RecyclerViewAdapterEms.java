package medpsu.doit.smart.fragments.ems.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import medpsu.doit.smart.R;

public class RecyclerViewAdapterEms extends RecyclerView.Adapter<RecyclerViewAdapterEms.ViewHolder> {

    private JSONArray mEmsList;

    private Context mContext;

    //กำหนดค่าที่รับเข้ามาจาก constructor ให้กับ list ก่อนที่จะไปใส่ใน component ต่างๆภายใน cardview
    public RecyclerViewAdapterEms(Context context, JSONArray trackList) {
        this.mEmsList = trackList;
        this.mContext = context;
    }

    //สร้าง viewHolder เพื่อให้สามารถอ้างถึง และ สามารถกำหนดค่าให้กับ component ต่างๆภายใน cardview ได้
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.format_ems_history, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    //ทำการ set value ให้กับ component ทั้งหมดที่อยู่ใน cardview
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
//        holder.mEmsTypeTextView.setText(mEmsList.get(position));

//        if (mEmsList.get(position).equals("พัสดุ")) {
//            holder.mEmsTypeImage.setImageResource(R.drawable.ic_ems);
//        } else if (mEmsList.get(position).equals("จดหมาย")) {
//            holder.mEmsTypeImage.setImageResource(R.drawable.ic_mail);
//        }
        try {
                final JSONObject jsonObject = new JSONObject(mEmsList.getJSONObject(position).toString());
                holder.mEmsTypeTextView.setText(jsonObject.get("track_post_no").toString());
                holder.mAddressContactTextView.setText(jsonObject.get("GDepname").toString());
                holder.mStartDateTextView.setText(jsonObject.get("date_rec").toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject jsonObject = new JSONObject(mEmsList.getJSONObject(position).toString());
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                    View dialogView = layoutInflater.inflate(R.layout.alertdialog_ems, null);

                    TextView emsNumber, addresContact, dateRecieveFromPost, recieverName, dateRecieved;
                    ImageView btnCloseDialog;

//                    ImageView imgEms;
//                    imgEms = dialogView.findViewById(R.id.img_ems);
//                    if (mEmsList.get(position).equals("พัสดุ")) {
//                        imgEms.setImageResource(R.drawable.ic_ems);
//                    } else if (mEmsList.get(position).equals("จดหมาย")) {
//                        imgEms.setImageResource(R.drawable.ic_mail);
//                    }

                    emsNumber = dialogView.findViewById(R.id.text_ems_number_value);
                    addresContact = dialogView.findViewById(R.id.text_address_contact_value);
                    dateRecieveFromPost = dialogView.findViewById(R.id.text_receive_date_from_post_value);
                    recieverName = dialogView.findViewById(R.id.text_receiver_name);
                    dateRecieved = dialogView.findViewById(R.id.text_receive_date_value);

                    String trackNo = jsonObject.get("track_post_no").toString();
                    String addressName = jsonObject.get("GDepname").toString();
                    String recieveFromPost = jsonObject.get("date_rec").toString();
                    String recieveDate = jsonObject.get("date_send").toString();
                    String firstName = jsonObject.get("EName").toString();
                    String surName = jsonObject.get("ESurname").toString();

                    if (!trackNo.equals("null")) {
                        emsNumber.setText(trackNo);
                    }else{
                        emsNumber.setText("-");
                    }

                    if (!addressName.equals("null")) {
                        addresContact.setText(addressName);
                    }else{
                        addresContact.setText("-");
                    }

                    if (!recieveFromPost.equals("null")) {
                        dateRecieveFromPost.setText(recieveFromPost);
                    }else{
                        dateRecieveFromPost.setText("-");
                    }

                    if (!recieveDate.equals("null")) {
                        dateRecieved.setText(recieveDate);
                    }else{
                        dateRecieved.setText("-");
                    }

                    if (!firstName.equals("null") && !surName.equals("null")) {
                        String name = firstName + " " + surName;
                        recieverName.setText(name);
                    } else{
                        recieverName.setText("-");
                    }

                    builder.setView(dialogView);

                    final AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogFade;
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    alertDialog.show();

                    btnCloseDialog = dialogView.findViewById(R.id.btn_dialog_close);
                    btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("Success", "Close Dialog");
                            alertDialog.dismiss();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEmsList.length();
    }

    //ทำการผูกตัวแปลเข้ากับ component ต่างๆใน cardview
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mEmsTypeTextView, mAddressContactTextView, mStartDateTextView;
        //        ImageView mEmsTypeImage;
        RelativeLayout parentLayout;

        public ViewHolder(View view) {
            super(view);
            mEmsTypeTextView = view.findViewById(R.id.text_ems_value);
            mAddressContactTextView = view.findViewById(R.id.text_address_contact_value);
            mStartDateTextView = view.findViewById(R.id.text_start_date_value);
            parentLayout = view.findViewById(R.id.relativelayout);
        }
    }
}
